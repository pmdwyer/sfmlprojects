#include <iostream>
#include <vector>

#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;

int main( int argc, char* argv[] )
{
  int width = 800, height = 600, index = -1;
  RenderWindow window( VideoMode( width, height ), "Bezier Curves!" ); 
  vector< sf::Vector2f > points;

  while ( window.isOpen() )
  {
    sf::Event event;
    while ( window.pollEvent( event ) )
    {
      switch ( event.type )
      {
        case sf::Event::Closed:
          window.close();
          break;

        case sf::Event::MouseButtonPressed:
          if ( event.mouseButton.button == sf::Mouse::Button::Left )
          {
            cout << "Left mouse button pressed" << endl;
            sf::Vector2f mouse( event.mouseButton.x, event.mouseButton.y );
            for ( int i = 0; i < points.size(); ++i )
            {
              sf::Vector2f dist = points[ i ] - mouse;
              cout << i << ": " << dist.x * dist.x + dist.y * dist.y << endl;
              if ( dist.x * dist.x + dist.y * dist.y < 100.0f )
              {
                cout << "i: " << i << endl;
                index = i;
              }
            }
            if ( index < 0 )
            {
              points.push_back( sf::Vector2f( event.mouseButton.x, event.mouseButton.y ) );
            }
          }
          break;

        case sf::Event::MouseButtonReleased:
          cout << "Left mouse button released" << endl;
          if ( event.mouseButton.button == sf::Mouse::Button::Left && index >= 0 )
          {
            points[ index ].x = event.mouseButton.x;
            points[ index ].y = event.mouseButton.y;
            index = -1;
          }
          else if ( event.mouseButton.button == sf::Mouse::Button::Right )
          {
            for ( auto itr = points.begin(); itr != points.end(); ++itr )
            {
              sf::Vector2f mouse( event.mouseButton.x, event.mouseButton.y );
              sf::Vector2f dist = *itr - mouse;
              if ( dist.x * dist.x + dist.y * dist.y < 100.0f )
              {
                points.erase( itr );
                break;
              }
            }
          }
          break;

        case sf::Event::KeyPressed:
          if ( event.key.code == sf::Keyboard::C )
          {
            points.clear();
          }
          break;

        default:
          break;
      }
    }

    vector< sf::Vertex > bs;
    for ( int i = 0; i < 1000 && points.size() > 1 ; ++i )
    {
      vector< sf::Vector2f > prevPoints( points );
      vector< sf::Vector2f > tempPoints( points.size() - 1 );

      while ( tempPoints.size() >= 2 )
      {
        for ( int j = 0; j < prevPoints.size() - 1; ++j )
        {
          tempPoints[ j ] = prevPoints[ j ] + ( ( float ) i / 1000.0f ) * ( prevPoints[ j + 1 ] - prevPoints[ j ] );
        }

        prevPoints.clear();
        prevPoints = tempPoints;
        tempPoints.pop_back();
      }
      Vector2f v = prevPoints[ 0 ] + ( ( float ) i / 1000.0f ) * ( prevPoints[ 1 ] - prevPoints[ 0 ] );
      bs.push_back( sf::Vertex( v, Color( 0, 255, 0 ) ) );
    }

    vector< sf::Vertex > ps( points.size() );
    for ( int i = 0; i < points.size(); ++i )
    {
      ps[ i ] = sf::Vertex( points[ i ], Color( 255, 0, 0 ) );
    }

    window.clear();

    if ( ps.size() > 1 )
    {
      window.draw( &ps[ 0 ], ps.size(), sf::LinesStrip );
    }
    if ( bs.size() > 1 )
    {
      window.draw( &bs[ 0 ], bs.size(), sf::LinesStrip );
    }

    window.display();

  }
  return 0;
}