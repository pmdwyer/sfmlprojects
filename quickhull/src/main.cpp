#include <random>
#include <vector>

#include <SFML\Graphics.hpp>

using namespace std;
using namespace sf;

vector< sf::Vector2f > QuickHull( const vector< sf::Vector2f >& points );
vector< sf::Vector2f > FindHull( const vector< sf::Vector2f >& points, sf::Vector2f a, sf::Vector2f b );
float Dot( sf::Vector2f a, sf::Vector2f b );

int main( int argc, char* argv[] )
{
  int width = 800, height = 600;
  RenderWindow window( VideoMode( width, height ), "Quickhull" );
  Event event;

  int numPoints = 100;
  vector< sf::Vector2f > points( numPoints );
  vector< sf::Vector2f > hullPoints;
  vector< sf::Vertex > vertices( numPoints );
  vector< sf::Vertex > hullLines;

  random_device device;
  mt19937 generator( device() );
  uniform_real_distribution< float > heightDist( height / 4.0f , 3.0f * ( height / 4.0f ) );
  uniform_real_distribution< float > widthDist( width / 4.0f,  3.0f * ( width / 4.0f ) );

  for ( int i = 0; i < numPoints; ++i )
  {
    points[ i ] = Vector2f( widthDist( generator ), heightDist( generator ) );
    vertices[ i ] = Vertex( points[ i ], Color( 0, 255, 0 ) );
  }

  hullPoints = QuickHull( points );
  for ( auto point : hullPoints )
  {
    hullLines.push_back( sf::Vertex( point, sf::Color::Blue ) );
  }

  while ( window.isOpen() )
  {
    while ( window.pollEvent( event ) )
    {
      switch ( event.type )
      {
        case sf::Event::Closed:
          window.close();
          break;

        default:
          break;
      }
    }

    window.clear();

    window.draw( &vertices[ 0 ], numPoints, sf::Points );
    window.draw( &hullLines[ 0 ], hullLines.size(), sf::LineStrip );

    window.display();
  }

  return 0;
}

vector< sf::Vector2f > QuickHull( const vector< sf::Vector2f >& points )
{
  float minIndex = 0, maxIndex = 0;
  vector< sf::Vector2f > hull, s1, s2;

  for ( int i = 1; i < points.size(); ++i )
  {
    if ( points[ i ].x < points[ minIndex ].x )
    {
      minIndex = i;
    }
    if ( points[ i ].x > points[ maxIndex ].x )
    {
      maxIndex = i;
    }
  }
  // need to assert minIndex != maxIndex
  float m = ( points[ maxIndex ].y - points[ minIndex ].y ) / ( points[ maxIndex ].x - points[ minIndex ].x );

  float c = m * points[ minIndex ].x - points[ minIndex ].y;
  
  for ( auto point : points )
  {
    if ( point == points[ minIndex ] || point == points[ maxIndex ] )
    {
      continue;
    }
    if ( point.y - m * point.x + c < 0 )
    {
      s1.push_back( point );
    }
    else
    {
      s2.push_back( point );
    }
  }

  hull.push_back( points[ minIndex ] );

  auto h1 = FindHull( s1, points[ minIndex ], points[ maxIndex ] );
  hull.insert( hull.end(), h1.begin(), h1.end() );

  hull.push_back( points[ maxIndex ] );

  auto h2 = FindHull( s2, points[ maxIndex ], points[ minIndex ] );
  hull.insert( hull.end(), h2.begin(), h2.end() );

  hull.push_back( points[ minIndex ] );

  return hull;
}

vector< sf::Vector2f > FindHull( const vector< sf::Vector2f >& points, sf::Vector2f left, sf::Vector2f right )
{
  vector< sf::Vector2f > hull, s1, s2;

  if ( points.size() <= 0 )
  {
    return hull;
  }

  sf::Vector2f v = right - left;
  sf::Vector2f vPerp;
  vPerp.x = -v.y;
  vPerp.y = v.x;
  int maxIndex = -1;
  float minD = FLT_MAX, rightMost = -FLT_MAX;

  for ( int i = 0; i < points.size(); ++i )
  {
    sf::Vector2f c = points[ i ] - left;
    float d = Dot( c, vPerp );
    float r = Dot( c, v );
    if ( d < minD || ( d == minD && r > rightMost ) )
    {
      minD = d;
      maxIndex = i;
      rightMost = r;
    }
  }

  // need to put triangle in right handed coordinate system
  //    A
  //  /   \
  // B --- C
  float y2y3 = points[ maxIndex ].y - right.y;
  float y1y3 = left.y - right.y;
  float y3y1 = right.y - left.y;
  float x1x3 = left.x - right.x;
  float x3x2 = right.x - points[ maxIndex ].x;
  float det = y2y3 * x1x3 + x3x2 * y1y3;
  for ( auto point : points )
  {
    if ( point == points[ maxIndex ] )
    {
      continue;
    }
    float l1 = ( ( y2y3 * ( point.x - right.x ) ) + ( x3x2 * ( point.y - right.y ) ) ) / det;
    float l2 = ( ( y3y1 * ( point.x - right.x ) ) + ( x1x3 * ( point.y - right.y ) ) ) / det;
    float l3 = 1.0f - l1 - l2;

    if ( l1 > 0 && l2 > 0 && l3 < 0 )
    {
      s1.push_back( point );
    }
    else if ( l1 > 0 && l2 < 0 && l3 < 0 )
    {
      s2.push_back( point );
    }
  }

  auto h1 = FindHull( s1, left, points[ maxIndex ] );
  hull.insert( hull.end(), h1.begin(), h1.end() );

  hull.push_back( points[ maxIndex ] );

  auto h2 = FindHull( s2, points[ maxIndex ], right );
  hull.insert( hull.end(), h2.begin(), h2.end() );
 
  return hull;
}

float Dot( sf::Vector2f a, sf::Vector2f b )
{
  return ( a.x * b.x + a.y * b.y );
}