#include "Ball.h"

using namespace pmd;

Ball::Ball( double radius, double mass )
  : m_radius{ radius },
    m_mass{ mass },
    m_vel{ 0.0, 0.0 },
    m_shape{ static_cast<float>( radius ) }
{
}

void Ball::SetVelocity( sf::Vector2f vel )
{
  m_vel = vel;
}

void Ball::PhysicsUpdate( double dt )
{
  sf::Vector2f dist = m_vel;
  dist.x *= dt;
  dist.y *= dt;
  Move( dist );
}

sf::FloatRect Ball::GetBounds() const
{
  return m_shape.getGlobalBounds();
}

void Ball::Move( const sf::Vector2f& offset )
{
  move( offset );
  m_shape.move( offset );
}

void Ball::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
  // states.transform *= getTransform();
  target.draw( m_shape, states );
}