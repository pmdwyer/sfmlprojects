#include <iostream>

#include <SFML/Graphics.hpp>

#include "Ball.h"
#include "Paddle.h"
#include "Block.h"
#include "GameState.h"

using namespace std;

float Dot( sf::Vector2f x, sf::Vector2f y )
{
  return x.x * y.x + x.y * y.y;
}

int main( int argc, char *argv[] )
{
  constexpr int width  = 800;
  constexpr int height = 600;
  constexpr int blockx = 5;
  constexpr int blocky = 4;

  sf::RenderWindow window( sf::VideoMode( width, height ), "escape" );

  pmd::Ball ball{ 10.0 };
  sf::Vector2f ballOrigin = { width / 2.0, height - 50.0 };
  ball.Move( ballOrigin );

  pmd::Paddle paddle{ 50.0, 10.0 };
  paddle.Move( { width / 2.0, height - 25.0 } );

  pmd::Block **blocks = new pmd::Block *[ blockx * blocky ];
  for ( int i = 0; i < blockx; ++i )
  {
    for ( int j = 0; j < blocky; ++j )
    {
      blocks[ i * blocky + j ] = new pmd::Block( 40.0, 10.0 );
      blocks[ i * blocky + j ]->Move( { width / 2.0f + i * 45.0f, height / 2.0f + j * 15.0f } );
    }
  }

  pmd::GameState state = pmd::GameState::WaitingToServe;
  sf::Clock clock;
  sf::Time dt;
  const sf::Vector2f norm = { 0.0f, 1.0f };
  const sf::Vector2f initialVel = { -200.0f, -200.0f };
  sf::Vector2f ballVel = initialVel;
  sf::Vector2f moveAmount;

  int lives = 3;
  int points = 0;

  while ( window.isOpen() && lives > 0 )
  {
    dt = clock.getElapsedTime();
    clock.restart();

    moveAmount.x = moveAmount.y = 0.0f;

    sf::Event event;
    while ( window.pollEvent( event ) )
    {
      if ( event.type == sf::Event::Closed )
        window.close();

      if ( event.type == sf::Event::KeyPressed )
      {
        switch ( event.key.code )
        {
          case sf::Keyboard::Left:
          case sf::Keyboard::A:
            moveAmount.x -= 10.0f;
            break;
          case sf::Keyboard::Right:
          case sf::Keyboard::D:
            moveAmount.x += 10.0f;
            break;
          case sf::Keyboard::Space:
            if ( state == pmd::GameState::WaitingToServe )
            {
              ball.SetVelocity( ballVel );
              state = pmd::GameState::Playing;
            }
            else if ( state == pmd::GameState::Playing )
            {
              state = pmd::GameState::WaitingToServe;
            }
            break;
        }
      }
    }

    // #physicsyo
    if ( state == pmd::GameState::Playing )
    {
      float seconds = dt.asSeconds();

      // Ghetto collision detection
      sf::Vector2f ballPos = ball.getPosition();
      sf::FloatRect ballBounds = ball.GetBounds();
      if ( ballBounds.intersects( paddle.GetBounds() ) )
      {
        sf::Vector2f newVel = ballVel - 2 * ( Dot( ballVel, norm ) ) * norm;
        ballVel = newVel;
        ball.SetVelocity( newVel );
      }

      for ( int i = 0; i < blockx * blocky; ++i )
      {
        if ( blocks[ i ] == nullptr )
          continue;

        if ( ballBounds.intersects( blocks[ i ]->GetBounds() ) )
        {
          sf::Vector2f n = norm;
          if ( ball.getPosition().x < blocks[ i ]->getPosition().x )
            n = -n;

          sf::Vector2f newVel = ballVel - 2 * ( Dot( ballVel, norm ) ) * norm;
          ballVel = newVel;
          ball.SetVelocity( newVel );

          points++;
          delete blocks[ i ];
          blocks[ i ] = nullptr;
        }
      }

      if ( ballPos.x < 0 || ballPos.x > width )
      {
        ballVel.x = -ballVel.x;
        ball.SetVelocity( ballVel );
      }

      if ( ballPos.y < 0 )
      {
        ballVel.y = -ballVel.y;
        ball.SetVelocity( ballVel );
      }
      else if ( ballPos.y > height )
      {
        // reset
        lives--;
        ballVel = initialVel;
        ball.Move( ballOrigin - ballPos );
        state = pmd::GameState::WaitingToServe;
      }

      // Movement
      paddle.Move( moveAmount );
      ball.PhysicsUpdate( seconds );
    }

    window.clear( sf::Color::Black );

    window.draw( ball );
    window.draw( paddle );
    for ( int i = 0; i < blockx * blocky; i++ )
      if ( blocks[ i ] )
        window.draw( *blocks[ i ] );

    window.display();
  }

  for ( int i = 0; i < blockx * blocky; i++ )
  {
    if ( blocks[ i ] )
      delete blocks[ i ];
  }
  delete [] blocks;

  return 0;
}