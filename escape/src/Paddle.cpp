#include "Paddle.h"

using namespace pmd;

Paddle::Paddle( double width, double height )
  : m_size{ static_cast<float>( width ), static_cast<float>( height ) },
    m_rect{ m_size }
{
}

sf::FloatRect Paddle::GetBounds() const
{
  return m_rect.getGlobalBounds();
}

void Paddle::Move( const sf::Vector2f& offset )
{
  move( offset );
  m_rect.move( offset );
}

void Paddle::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
  target.draw( m_rect, states );
}