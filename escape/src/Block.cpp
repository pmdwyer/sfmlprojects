#include "Block.h"

using namespace pmd;

Block::Block( double width, double height, int hits )
  : m_hits{ hits },
    m_size{ static_cast<float>( width ), static_cast<float>( height ) },
    m_rect{ m_size }
{
}

sf::FloatRect Block::GetBounds() const
{
  return m_rect.getGlobalBounds();
}

void Block::Move( const sf::Vector2f& offset )
{
  move( offset );
  m_rect.move( offset );
}

void Block::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
  // states.transform *= getTransform();
  target.draw( m_rect, states );
}