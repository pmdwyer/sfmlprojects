#ifndef _PMD_BALL
#define _PMD_BALL

#include <vector>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

namespace pmd
{
  class Ball : public sf::Transformable, public sf::Drawable
  {
    public:
      Ball( double radius, double mass = 10.0 );

      void SetVelocity( sf::Vector2f acc );
      void PhysicsUpdate( double dt );
      sf::FloatRect GetBounds() const;

      void Move(const sf::Vector2f& offset);

    private:
      double          m_radius;
      double          m_mass;
      sf::Vector2f    m_vel;
      sf::CircleShape m_shape;

      std::vector<sf::Vector2f> m_forces;

      void draw( sf::RenderTarget& target, sf::RenderStates states ) const override;
  };
}

#endif // _PMD_BALL