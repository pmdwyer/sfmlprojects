#ifndef _PMD_GAMESTATE
#define _PMD_GAMESTATE

namespace pmd
{
  enum class GameState
  {
    WaitingToServe,
    Playing
  };
}

#endif // _PMD_GAMESTATE