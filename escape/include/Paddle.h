#ifndef _PMD_PADDLE
#define _PMD_PADDLE

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

namespace pmd
{
  class Paddle : public sf::Transformable, public sf::Drawable
  {
    public:
      Paddle( double width, double height );

      sf::FloatRect GetBounds() const;
      void Move( const sf::Vector2f& offset );

    private:
      sf::Vector2f        m_size;
      sf::RectangleShape  m_rect;

      void draw( sf::RenderTarget& target, sf::RenderStates states ) const override;
  };
}

#endif // _PMD_PADDLE