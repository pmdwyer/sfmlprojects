#ifndef _PMD_BLOCK
#define _PMD_BLOCK

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

namespace pmd
{
  class Block : public sf::Transformable, public sf::Drawable
  {
    public:
      Block( double width, double height, int hits = 1 );

      sf::FloatRect GetBounds() const;
      void Move( const sf::Vector2f& offset );

    private:
      int m_hits = 1;
      sf::Vector2f m_size;
      sf::RectangleShape m_rect;

      void draw( sf::RenderTarget& target, sf::RenderStates states ) const override;
  };
}

#endif // _PMD_BLOCK