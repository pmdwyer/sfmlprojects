#include <iostream>
#include <vector>

#include <SFML/Graphics.hpp>
#include <zinnia.h>

using namespace std;
using namespace sf;

int main(int argc, char *argv[]) {
    int width = 1200, height = 800, index = -1;
    RenderWindow window(VideoMode(width, height), "Kanji Recognizer!");
    vector<sf::Vector2f> line;
    vector<vector<sf::Vector2f>> lines;
    bool pressed = false;

    zinnia_recognizer_t *recognizer = zinnia_recognizer_new();
    if (!zinnia_recognizer_open(recognizer, "/home/patrick/projects/zinnia/zinnia-tomoe-model/handwriting-ja.model")) {
        fprintf(stderr, "ERROR1: %s\n", zinnia_recognizer_strerror(recognizer));
        return -1;
    }

    auto *character = zinnia_character_new();
    zinnia_character_set_height(character, height);
    zinnia_character_set_width(character, width);
    while (window.isOpen()) {
        sf::Event event{};
        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:
                    window.close();
                    break;
                case sf::Event::MouseButtonPressed:
                    if (event.mouseButton.button == sf::Mouse::Button::Left) {
                        std::cout << "pressed" << std::endl;
                        pressed = true;
                        line.clear();
                    }
                    break;
                case sf::Event::MouseButtonReleased:
                    if (event.mouseButton.button == sf::Mouse::Button::Left) {
                        std::cout << "released" << std::endl;
                        pressed = false;
                        lines.push_back(line);
                    }
                    break;
                case sf::Event::KeyPressed:
                    if (event.key.code == sf::Keyboard::C) {
                        lines.clear();
                    }
                    break;
                case sf::Event::MouseMoved:
                    if (pressed) {
                        line.emplace_back(event.mouseMove.x, event.mouseMove.y);
                    }
                default:
                    break;
            }
        }
        zinnia_character_clear(character);
        for (int i = 0; i < lines.size(); i++) {
            for (int j = 0; j < lines[i].size(); j += 2) {
                zinnia_character_add(character, i, static_cast<int>(lines[i][j].x), static_cast<int>(lines[i][j].y));
            }
        }

        window.clear();
        for (int i = 0; i < lines.size(); i++) {
            vector<sf::Vertex> ps(lines[i].size());
            for (int j = 0; j < lines[i].size(); j++) {
                ps[j] = Vertex(lines[i][j], Color::Green);
            }
            window.draw(&ps[0], ps.size(), sf::LineStrip);
        }
        window.display();

        auto *result = zinnia_recognizer_classify(recognizer, character, 10);
        if (!result) {
            continue;
        }
        std::cout << "results: " << endl;
        for (size_t i = 0; i < zinnia_result_size(result); ++i) {
            std::cout << zinnia_result_value(result, i) << "\t" << zinnia_result_score(result, i) << std::endl;
        }
        zinnia_result_destroy(result);
    }
    return 0;
}
