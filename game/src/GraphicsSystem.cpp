#include "GraphicsSystem.h"

game::GraphicsSystem::GraphicsSystem() : _graphicComponents(), _mainCamera()
{

}

game::GraphicsSystem::~GraphicsSystem()
{

}

void game::GraphicsSystem::Render( sf::RenderTarget& target )
{
  target.setView( _mainCamera.GetView() );
  for ( auto graphic : _graphicComponents )
  {
    graphic->Draw( target );
  }
}

void game::GraphicsSystem::Create( const std::string& type, std::shared_ptr< game::Entity > parent, const game::ComponentParameters& p )
{
  if ( type.compare( typeid( CircleGraphic ).name() ) == 0 )
  {
    std::shared_ptr< CircleGraphic > cg( new CircleGraphic( *this, _graphicComponents.size() ) );
    _graphicComponents.push_back( cg );
    parent->AddComponent( cg );
    cg->SetParent( parent );
    // init data
    const CircleGraphicParameters& params = static_cast< const CircleGraphicParameters& >( p );
    cg->SetSize( params.radius );
    cg->SetColor( params.color );
  }
  else if ( type.compare( typeid( BoxGraphic ).name() ) == 0 )
  {
    std::shared_ptr< BoxGraphic > bg( new BoxGraphic( *this, _graphicComponents.size() ) );
    _graphicComponents.push_back( bg );
    parent->AddComponent( bg );
    bg->SetParent( parent );

    const BoxGraphicParameters& params = static_cast< const BoxGraphicParameters& >( p );
    bg->SetSize( params.size );
    bg->SetColor( params.color );
  }
}

void game::GraphicsSystem::Remove( ComponentId cid )
{
  _graphicComponents.erase( _graphicComponents.begin() + cid );
}