#include "RigidBody.h"

float game::RigidBody::EPSILON = std::numeric_limits< float >::epsilon();

game::RigidBody::RigidBody( System& system, ComponentId id )
  : Component( typeid( RigidBody ).name(), system, id ), _acceleration( 0.f, 0.f ), _velocity( 0.f, 0.f ), _forceAccum( 0.0f, 0.0f )
{
  _rbType = RigidBodyType::PhysicsBody;
  _shape = ColliderShape::None;
  _collider = nullptr;
  _friction = 1.0f;
  _mass = 1.0f;
  _invMass = 1.0f;
  _damping = 0.2f;
}

game::RigidBody::~RigidBody()
{

}

void game::RigidBody::SetMass( float mass )
{
  _mass = mass;
  if ( _mass != 0.0f )
  {
    _invMass = 1.0f / mass;
    _rbType = RigidBodyType::PhysicsBody;
  }
  else
  {
    _rbType = RigidBodyType::GameBody;
  }
}

void game::RigidBody::SetDamping( float damp )
{
  _damping = damp;
}

void game::RigidBody::SetBodyType( RigidBodyType rbType )
{
  _rbType = rbType;
}

void game::RigidBody::SetFriction( float friction )
{
  _friction = friction;
}

void game::RigidBody::SetColliderShape( game::ColliderShape shape, const ColliderParameters& params )
{
  _shape = shape;
  switch ( _shape )
  {
    case ColliderShape::None:
      _collider = nullptr;
      break;
    case ColliderShape::AABB:
    {
      std::shared_ptr< AABBCollider > box( new AABBCollider( _parent ));
      const AABBColliderParameters &bp( static_cast< const AABBColliderParameters & > ( params ));
      box->SetExtents( bp.widthHeight );
      box->SetCenter( bp.center );
      _collider = std::dynamic_pointer_cast< Collider >( box );
      break;
    }
    case ColliderShape::Circle:
    {
      std::shared_ptr< CircleCollider > circle( new CircleCollider( _parent ));
      const CircleColliderParameters &circleParams( static_cast< const CircleColliderParameters & >( params ));
      circle->SetRadius( circleParams.radius );
      _collider = std::dynamic_pointer_cast< Collider >( circle );
      break;
    }
  }
}

sf::Vector2f game::RigidBody::GetVelocity() const
{
  return _velocity;
}

sf::Vector2f game::RigidBody::GetAcceleration() const
{
  return _acceleration;
}

sf::Transformable game::RigidBody::GetTransformable() const
{
  return _parent->transform;
}

float game::RigidBody::GetInvMass() const
{
  return _invMass;
}
const game::RigidBodyType game::RigidBody::GetBodyType() const
{
  return _rbType;
}

void game::RigidBody::Update( float dt )
{
  sf::Vector2f oldAcceleration = _acceleration;
  sf::Vector2f oldVelocity = _velocity;

  // update position
  _parent->transform.move( _velocity * dt + ( 0.5f * oldAcceleration * ( dt * dt ) ) );

  // cacluate new acceleration
  _acceleration = _forceAccum * _invMass;
  _forceAccum.x = _forceAccum.y = 0.0f;

  // avoid floating point errors
  if ( _acceleration.x < EPSILON && _acceleration.x > -EPSILON )
  {
    _acceleration.x = 0.0f;
  }
  if ( _acceleration.y < EPSILON && _acceleration.y > -EPSILON )
  {
    _acceleration.y = 0.0f;
  }

  // calculate new velocity
  _velocity += ( ( ( oldAcceleration + _acceleration ) / 2.0f ) * dt );
  _velocity.x *= pow( _damping, dt );
  _velocity.y *= pow( _damping, dt );
}

void game::RigidBody::AddImpulseForce( const sf::Vector2< float >& force )
{
  _forceAccum += force;
}

game::ColliderShape game::RigidBody::GetShape() const
{
  return _shape;
}

const game::Collider &game::RigidBody::GetCollider() const
{
  return *_collider;
}
