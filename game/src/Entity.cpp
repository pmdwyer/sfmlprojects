#include "Entity.h"
#include "Component.h"
#include "Scene.h"

game::Entity::Entity( Scene& s, EntityId id, std::string tag ) 
  : _owningScene( s ), _id( id ), _tag( tag ), _transform(), _parent( nullptr ), _children(), _components()
{

}

game::Entity::~Entity()
{

}

std::shared_ptr< game::Component > game::Entity::GetComponent( std::string type )
{
  for ( auto comp : _components )
  {
    if ( comp->GetType().compare( type ) == 0 )
    {
      return comp;
    }
  }
  return nullptr;
}

void game::Entity::AddComponent( std::shared_ptr< game::Component > comp )
{
  _components.push_back( comp );
}

void game::Entity::Destroy()
{
  // destroy all children
  for ( auto child : _children )
  {
    child->Destroy();
  }

  // remove all components
  for ( auto comp : _components )
  {
    comp->Delete();
  }

  _children.clear();
  _components.clear();

  // remove this from the scene
  _owningScene.Remove( _id );
}

sf::Transform game::Entity::CalcTransform()
{
  if ( _parent == nullptr )	// we have no parent
  {
    return _transform.getTransform();
  }
  return ( _transform.getTransform() * _parent->CalcTransform() );
}

sf::Transformable game::Entity::CalcTransformable()
{
  if ( _parent == nullptr )
  {
    return _transform;
  }
  return _parent->CalcTransformable();
}

void game::Entity::AddChild( std::shared_ptr< game::Entity > child )
{
  child->_parent = this;
  _children.push_back( child );
}

void game::Entity::RemoveChild( std::string tag )
{
  for ( size_t i = 0; i < _children.size(); ++i )
  {
    if ( _children[ i ]->GetTag() == tag )
    {
      _children[ i ]->_parent = nullptr;
      _children.erase( _children.begin() + i );
    }
  }
}

std::shared_ptr< game::Entity > game::Entity::GetChild( std::string tag )
{
  std::shared_ptr< Entity > c = nullptr;
  for ( auto child : _children )
  {
    if ( child->GetTag() == tag )
    {
      c = child;
    }
  }
  return c;
}
