#include "CircleCollider.h"

game::CircleCollider::CircleCollider( std::shared_ptr< game::Entity > _parent )
  : Collider( _parent )
{
  _radius = 10.0f;
}

game::CircleCollider::~CircleCollider()
{
}

float game::CircleCollider::GetRadius() const
{
  return _radius;
}

void game::CircleCollider::SetRadius( float rad )
{
  _radius = rad;
}