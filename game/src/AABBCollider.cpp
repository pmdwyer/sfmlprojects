#include "AABBCollider.h"

game::AABBCollider::AABBCollider( std::shared_ptr< Entity > parent )
  : Collider( parent ), _center( 0.0, 0.0 ), _halfWidthHeight( 0.5, 0.5 )
{
}

const sf::Vector2f game::AABBCollider::GetExtents() const
{
  sf::Vector2f wh = _halfWidthHeight;
  wh.x *= 2.0;
  wh.y *= 2.0;
  return wh;
}

const sf::Vector2f game::AABBCollider::GetCenter() const
{
  return _center;
}

void game::AABBCollider::SetExtents( sf::Vector2f extents )
{
  _halfWidthHeight.x = extents.x / 2.0f;
  _halfWidthHeight.y = extents.y / 2.0f;
}

void game::AABBCollider::SetCenter( sf::Vector2f center )
{
  _center = center;
}

const sf::Vector2f game::AABBCollider::GetPosition() const
{
  sf::Transform t = _parent->CalcTransform();
  t.translate( _center );
  return t.getPosition();
}
