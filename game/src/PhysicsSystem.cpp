#include "PhysicsSystem.h"

game::PhysicsSystem::PhysicsSystem() : _rigidBodies()
{

}

game::PhysicsSystem::~PhysicsSystem()
{

}

void game::PhysicsSystem::Step( float time )
{
  // detect and resolve collisions
  for ( unsigned int i = 0; i < _rigidBodies.size(); ++i )
  {
    for ( unsigned int j = i + 1; j < _rigidBodies.size(); ++j )
    {
      // detected a collision
      if ( Collides( *_rigidBodies[ i ], *_rigidBodies[ j ] ) )
      {
        // check for triggers first, activate those if any, otherwise resolve normal collision
        if ( _rigidBodies[ i ]->GetCollider().IsTrigger() )
        {
//          _rigidBodies[ i ]->GetCollider().OnTrigger( _rigidBodies[ j ]->GetCollider()._parent );
        }
        if ( _rigidBodies[ j ]->GetCollider().IsTrigger() )
        {

        }
        Resolve( *_rigidBodies[ i ], *_rigidBodies[ j ] );
      }
    }
  }

  // update the velocity and acceleration
  for ( auto body : _rigidBodies )
  {
    body->Update( time );
  }
}

void game::PhysicsSystem::Create( const std::string& type, std::shared_ptr< Entity > parent, const ComponentParameters& params )
{
  if ( type.compare( typeid( RigidBody ).name() ) == 0 )
  {
    std::shared_ptr< RigidBody > rb( new RigidBody( *this, _rigidBodies.size() ) );
    _rigidBodies.push_back( rb );
    rb->SetParent( parent );
    parent->AddComponent( rb );

    const RigidBodyParameters& p( static_cast< const RigidBodyParameters& >( params ) );
    rb->SetMass( p.mass );
    rb->SetFriction( p.friction );
    rb->SetDamping( p.damping );
    rb->SetBodyType( p.type );
    rb->SetColliderShape( p.shape, *p.colliderParams );
  }
}

void game::PhysicsSystem::Remove( ComponentId cid )
{
  // need to separate out types so that we can remove the correct one
  // or we need to also get type information somehow
  //_rigidBodies.erase( _rigidBodies.begin() + cid );
}

bool game::PhysicsSystem::Collides( const game::RigidBody &a, const game::RigidBody &b )
{
  bool collided = false;
  switch ( a.GetShape() )
  {
    case ColliderShape::None:
      break;
    case ColliderShape::Circle:
      switch ( b.GetShape() )
      {
        case ColliderShape::None:
          break;
        case ColliderShape::Circle:
          // circle circle
          collided = Collides( static_cast< const CircleCollider& > ( a.GetCollider() ),
                               static_cast< const CircleCollider& > ( b.GetCollider() ) );
          break;
        case ColliderShape::AABB:
          // circle aabb
          collided = Collides( static_cast< const CircleCollider& > ( a.GetCollider() ),
                               static_cast< const AABBCollider& > ( b.GetCollider() ) );
          break;
      }
      break;
    case ColliderShape::AABB:
      switch ( b.GetShape() )
      {
        case ColliderShape::None:
          break;
        case ColliderShape::Circle:
          // circle aabb
          collided = Collides( static_cast< const CircleCollider& > ( b.GetCollider() ),
                               static_cast< const AABBCollider& > ( a.GetCollider() ) );
          break;
        case ColliderShape::AABB:
          // aabb aabb
          collided = Collides( static_cast< const AABBCollider& > ( a.GetCollider() ),
                               static_cast< const AABBCollider& > ( b.GetCollider() ) );
          break;
      }
      break;
  }
  return collided;
}

bool game::PhysicsSystem::Collides( const game::CircleCollider &a, const game::CircleCollider &b )
{
	float r = a.GetRadius() + b.GetRadius();
	r *= r;
	sf::Vector2f dist = ( b.GetPosition() - a.GetPosition() );
	return r > ( ( dist.x * dist.x ) + ( dist.y * dist.y ) );
}

bool game::PhysicsSystem::Collides( const game::CircleCollider &a, const game::AABBCollider &b )
{
  sf::Vector2f dist = ( a.GetPosition() - b.GetPosition() );
  dist.x = abs( dist.x );
  dist.y = abs( dist.y );
  sf::Vector2f wh = b.GetExtents();
  float r = a.GetRadius();

  if ( dist.x > ( r + wh.x / 2 ) )
    return false;
  if ( dist.y > ( r + wh.y / 2 ) )
    return false;

  if ( dist.x <= ( wh.x / 2 ) )
    return true;
  if ( dist.y <= ( wh.y / 2 ) )
    return true;

  double cornerSquareDist = pow( ( dist.x - ( wh.x / 2 ) ), 2.0 ) + pow( ( dist.y - ( wh.y /2 ) ), 2.0 );

  return cornerSquareDist <= r * r;
}

bool game::PhysicsSystem::Collides( const game::AABBCollider &a, const game::AABBCollider &b )
{
  return false;
}

void game::PhysicsSystem::Resolve( game::RigidBody& a, game::RigidBody& b )
{
  std::cout << "Collided!" << std::endl;
  // get the relative velocity
  sf::Vector2f relativeVel = b.GetVelocity() - a.GetVelocity();

  // calculate the impact angle / normal
  sf::Vector2f normal = b.GetTransform().getPosition() - a.GetTransform().getPosition();

  float scale = relativeVel.x * normal.x + relativeVel.y * normal.y;

  if ( scale >= 0 )
  {
    return;
  }

  scale *= -1;
  scale /= a.GetInvMass() + b.GetInvMass();

  sf::Vector2f impulse = scale * normal;
  if ( a.GetBodyType() == RigidBodyType::PhysicsBody )
    a.AddImpulseForce( -impulse );
  if ( b.GetBodyType() == RigidBodyType::PhysicsBody )
  b.AddImpulseForce( impulse );
}