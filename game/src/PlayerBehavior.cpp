#include "PlayerBehavior.h"

game::PlayerBehavior::PlayerBehavior( System& system, ComponentId id )
  : Behavior( typeid( PlayerBehavior ).name(), system, id )
{
}

game::PlayerBehavior::~PlayerBehavior()
{
}

void game::PlayerBehavior::Start()
{
  _ball = _parent->GetChild( "Ball" );
  _rigidBody = std::dynamic_pointer_cast< RigidBody > ( _parent->GetComponent( typeid( RigidBody ).name() ) );
  assert( _rigidBody != nullptr );
  // probably need to gracefully handle missing rigid bodies
}

void game::PlayerBehavior::Update( float dt )
{
  sf::Vector2< float > f;
  float force = 10000.0f;
  if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Up ) )
  {
    f.y = -force;
    _rigidBody->AddImpulseForce( f );
  }
  if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Down ) )
  {
    f.y = force;
    _rigidBody->AddImpulseForce( f );
  }
  if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Left ) )
  {
    f.x = -force;
    _rigidBody->AddImpulseForce( f );
  }
  if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Right ) )
  {
    f.x = force;
    _rigidBody->AddImpulseForce( f );
  }
  if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Space ) )
  {
    //_rigidBody->GetVelocity();
  }
}