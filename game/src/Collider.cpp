#include "Collider.h"

game::Collider::Collider( std::shared_ptr< Entity > parent )
  : _isTrigger( false ), _parent( parent )
{
}

game::Collider::~Collider()
{
}

void game::Collider::SetTrigger( const Trigger& trigger )
{
  if ( trigger != nullptr ) 
  {
    _trigger = trigger;
    _isTrigger = true;
  }
}

void game::Collider::OnTrigger( std::shared_ptr< Entity >& other )
{
  if ( _isTrigger && _trigger != nullptr )
  {
    _trigger( this->_parent, other );
  }
}

bool game::Collider::IsTrigger() const
{
  return _isTrigger;
}

const sf::Vector2f game::Collider::GetPosition() const
{
  return _parent->transform.getPosition();
}
