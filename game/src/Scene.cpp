#include "Scene.h"

#include "CircleGraphic.h"
#include "PlayerBehavior.h"
#include "EnemyTrigger.h"

int game::Scene::_numTotalScenes = 0;

game::Scene::Scene() : _entities(), _graphics(), _behaviors()
{
  _numTotalScenes++;

  // set up player
  std::shared_ptr<Entity> player = CreateEntity( "Player" );
  player->transform.move( -200.f, 0.f );

  BoxGraphicParameters bgp;
  bgp.size = sf::Vector2f( 50.0, 50.0 );
  bgp.color = sf::Color::Green;
  _graphics.Create( typeid( BoxGraphic ).name(), player, bgp );

  RigidBodyParameters rbp;
  std::shared_ptr< AABBColliderParameters > aabbp( new AABBColliderParameters() );
  aabbp->widthHeight = sf::Vector2f( 50.0, 50.0 );
  rbp.friction = 0.1f;
  rbp.mass = 5.0f;
  rbp.shape = ColliderShape::AABB;
  rbp.type = RigidBodyType::GameBody;
  rbp.colliderParams = aabbp;
  _physics.Create( typeid( RigidBody ).name(), player, rbp );

  BehaviorParameters bp;
  _behaviors.Create( typeid( PlayerBehavior ).name(), player, bp );

  // set up ball
  std::shared_ptr< Entity > ball = CreateEntity( "Ball" );
  ball->transform.move( 100.0f, 0.f );

  CircleGraphicParameters cgp;
  cgp.radius = 10.f;
  cgp.color = sf::Color::Blue;
  _graphics.Create( typeid( CircleGraphic ).name(), ball, cgp );

  std::shared_ptr< CircleColliderParameters > bccp( new CircleColliderParameters() );
  bccp->radius = 10.0;
  rbp.type = RigidBodyType::GameBody;
  rbp.shape = ColliderShape::Circle;
  rbp.colliderParams = bccp;
  _physics.Create( typeid( RigidBody ).name(), ball, rbp );

  player->AddChild( ball );

  // set up enemy
  std::shared_ptr<Entity> enemy = CreateEntity( "Enemy" );
  //enemy->transform.move( 50.f, 0.f );

  cgp;
  cgp.radius = 10.f;
  cgp.color = sf::Color::Red;
  _graphics.Create( typeid( CircleGraphic ).name(), enemy, cgp );

  // use the same rigid body parameters as the player
  std::shared_ptr< CircleColliderParameters > ccp( new CircleColliderParameters() );
  ccp->radius = 10.0;
  rbp.type = RigidBodyType::PhysicsBody;
  rbp.shape = ColliderShape::Circle;
  rbp.colliderParams = ccp;
  _physics.Create( typeid( RigidBody ).name(), enemy, rbp );
}

game::Scene::Scene( std::string fname ) : _entities(), _graphics(), _behaviors()
{
  _numTotalScenes++;
  Load( fname );
}

game::Scene::~Scene()
{
  _numTotalScenes--;
  _deadEntities.clear();
  _entities.clear();
}

void game::Scene::Load( std::string fname )
{

}

void game::Scene::Init()
{
  _behaviors.Start();
}

void game::Scene::Update( float secs )
{
  _behaviors.Update( secs );
}

void game::Scene::PhysicsUpdate( float secs )
{
  _physics.Step( secs );
}

void game::Scene::Draw( sf::RenderTarget& target )
{
  sf::RectangleShape xaxis( sf::Vector2f( 200, 1 ) );
  sf::RectangleShape yaxis( sf::Vector2f( 1, 200 ) );

  _graphics.Render( target );

  target.draw( xaxis );
  target.draw( yaxis );
}

void game::Scene::HandleInput( sf::Event& event )
{

}

void game::Scene::Remove( EntityId eid )
{
  _deadEntities.push_back( _entities[ eid ] );
  _entities.erase( _entities.begin() + eid );
}

std::shared_ptr< game::Entity > game::Scene::CreateEntity( std::string tag )
{
  Entity* e = new Entity( *this, _entities.size(), tag );
  std::shared_ptr< Entity > entity( e );
  _entities.push_back( entity );
  return _entities[ _entities.size() - 1 ];
}