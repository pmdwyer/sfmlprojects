#include "BoxGraphic.h"

game::BoxGraphic::BoxGraphic( System & system, ComponentId id )
  : GraphicComponent( typeid( BoxGraphic ).name(), system, id ), _box(), _halfSize()
{
}

void game::BoxGraphic::Draw( sf::RenderTarget& target )
{
  sf::Transform t = _parent->CalcTransform().translate( -_halfSize );
  sf::RenderStates state( t );
  target.draw( _box, state );
}

void game::BoxGraphic::SetSize( const sf::Vector2f & v )
{
  _box.setSize( v );
  _halfSize.x = v.x / 2.0f;
  _halfSize.y = v.y / 2.0f;
}

void game::BoxGraphic::SetColor( const sf::Color & color )
{
  _box.setFillColor( color );
  _box.setOutlineColor( color );
}
