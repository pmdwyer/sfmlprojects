#include "CircleGraphic.h"

game::CircleGraphic::CircleGraphic( System& system, ComponentId id )
  : GraphicComponent( typeid( CircleGraphic ).name(), system, id ), _circle(), _halfRadius()
{
}

game::CircleGraphic::~CircleGraphic()
{
}

void game::CircleGraphic::SetSize( float radius )
{
  _circle.setRadius( radius );
  sf::Vector2f temp( radius / 2.0f, radius / 2.0f );
  _halfRadius.x = sqrt( temp.x * temp.x + temp.y * temp.y );
  _halfRadius.y = _halfRadius.x;
}

float game::CircleGraphic::GetSize()
{
  return _circle.getRadius();
}

void game::CircleGraphic::SetColor( const sf::Color color )
{
  _circle.setFillColor( color );
}

sf::Color game::CircleGraphic::GetColor()
{
  return _circle.getFillColor();
}

void game::CircleGraphic::Draw( sf::RenderTarget& target )
{
  sf::Transform t = _parent->CalcTransform().translate( -_halfRadius );
  sf::RenderStates states( t );
  target.draw( _circle, states );
}