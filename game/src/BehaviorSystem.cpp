#include "BehaviorSystem.h"
#include "PlayerBehavior.h"

game::BehaviorSystem::BehaviorSystem()
{

}

game::BehaviorSystem::~BehaviorSystem()
{

}

void game::BehaviorSystem::Start()
{
  for ( auto behavior : _behaviors )
  {
    behavior->Start();
  }
}

void game::BehaviorSystem::Update( float deltaTime )
{
  for ( auto behavior : _behaviors )
  {
    behavior->Update( deltaTime );
  }
}

void game::BehaviorSystem::Create( const std::string& type, std::shared_ptr< game::Entity > parent, const game::ComponentParameters& params )
{
  if ( type == typeid( PlayerBehavior ).name() )
  {
    std::shared_ptr< PlayerBehavior > behavior( new PlayerBehavior( *this, _behaviors.size() ) );
    behavior->SetParent( parent );
    parent->AddComponent( behavior );
    _behaviors.push_back( behavior );
  }
}

void game::BehaviorSystem::Remove( ComponentId cid )
{
  _behaviors.erase( _behaviors.begin() + cid );
}