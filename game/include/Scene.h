#ifndef __GAME_SCENE
#define __GAME_SCENE

#include <memory>
#include <string>
#include <vector>

#include <SFML/Graphics.hpp>

#include "BehaviorSystem.h"
#include "Entity.h"
#include "GraphicsSystem.h"
#include "PhysicsSystem.h"

namespace game
{
  class Scene
  {
  public:
    Scene();
    Scene( std::string fname );
    ~Scene();

    void Load( std::string fname );

    void Init();

    void Update( float secs );
    void PhysicsUpdate( float secs );
    void Draw( sf::RenderTarget& target );
    void HandleInput( sf::Event& event );
    void Remove( EntityId entityId );

  private:
    std::vector<std::shared_ptr< Entity > > _entities;
    std::vector< std::shared_ptr< Entity > > _deadEntities;
    std::string _title;

    GraphicsSystem _graphics;
    BehaviorSystem _behaviors;
    PhysicsSystem _physics;

    static int _numTotalScenes;
  
    std::shared_ptr< Entity > CreateEntity( std::string tag );
  };
}

#endif // __GAME_SCENE