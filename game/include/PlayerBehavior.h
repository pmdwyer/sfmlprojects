#ifndef __GAME_PLAYERBEHAVIOR
#define __GAME_PLAYERBEHAVIOR

#include <iostream>
#include <memory>

#include <SFML/Window/Keyboard.hpp>

#include "Behavior.h"
#include "RigidBody.h"

namespace game
{
	class PlayerBehavior : public Behavior
	{
	public:
		PlayerBehavior( System& system, ComponentId id );
		~PlayerBehavior();

		void Start() override;
		void Update( float dt ) override;

	private:
		std::shared_ptr< Entity > _ball;
		std::shared_ptr< RigidBody > _rigidBody;
	};
}

#endif