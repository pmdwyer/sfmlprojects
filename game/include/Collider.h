#ifndef __GAME_COLLIDERCOMPONENT
#define __GAME_COLLIDERCOMPONENT

#include <memory>
#include <string>
#include <functional>

#include "Entity.h"

namespace game
{
  typedef std::function< void( std::shared_ptr< Entity >&, std::shared_ptr< Entity >& ) > Trigger;

  struct ColliderParameters
  {
    ColliderParameters() : trigger( nullptr ) {};
    std::unique_ptr< Trigger > trigger;
  };

  class Collider
  {
  public:
    friend class PhysicsSystem;

    Collider( std::shared_ptr< Entity > _parent );
    virtual ~Collider();

    void SetTrigger( const Trigger& trigger );
    void OnTrigger( std::shared_ptr< Entity >& other );
    bool IsTrigger() const;

    virtual const sf::Vector2f GetPosition() const;

  protected:
    std::shared_ptr< Entity > _parent;
    bool _isTrigger;
    Trigger _trigger;
  };
}

#endif // __GAME_COLLIDERCOMPONENT