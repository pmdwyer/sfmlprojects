#ifndef __GAME_BOXGRAPHIC
#define __GAME_BOXGRAPHIC

#include <SFML/Graphics/RectangleShape.hpp>

#include "GraphicComponent.h"

namespace game
{
  struct BoxGraphicParameters : public ComponentParameters
  {
    BoxGraphicParameters() : ComponentParameters(), size( 1.0, 1.0 ), color( sf::Color::White ) {};
    sf::Vector2f size;
    sf::Color color;
  };

  class BoxGraphic : public GraphicComponent
  {
  public:
    BoxGraphic( System& system, ComponentId id );

    void Draw( sf::RenderTarget& target ) override;

    void SetSize( const sf::Vector2f& v );
    void SetColor( const sf::Color& color );

  private:
    sf::Vector2f _halfSize;
    sf::RectangleShape _box;
  };
}

#endif // __GAME_BOXGRAPHIC
