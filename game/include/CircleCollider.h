#ifndef __GAME_CIRCLECOLLIDER
#define __GAME_CIRCLECOLLIDER

#include "Entity.h"
#include "Collider.h"

namespace game
{
  struct CircleColliderParameters : public ColliderParameters
  {
    CircleColliderParameters() : radius( 1.0f ), ColliderParameters() {};
    float radius;
  };

  class CircleCollider : public Collider
  {
  public:
    CircleCollider( std::shared_ptr< game::Entity > _parent );
    ~CircleCollider();

    float GetRadius() const;
    void SetRadius( float rad );

  private:
    float _radius;
  };
}

#endif // __GAME_CIRCLECOLLIDER