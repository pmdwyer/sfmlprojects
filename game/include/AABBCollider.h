#ifndef __GAME_AABBCOLLIDER
#define __GAME_AABBCOLLIDER

#include "Collider.h"

namespace game
{
  struct AABBColliderParameters : public ColliderParameters
  {
    AABBColliderParameters() : ColliderParameters(), center( 0.0, 0.0 ), widthHeight( 1.0, 1.0 ) {};
    sf::Vector2f center;
    sf::Vector2f widthHeight;
  };

  class AABBCollider : public Collider
  {
  public:
    AABBCollider( std::shared_ptr< Entity > parent );

    const sf::Vector2f GetExtents() const;
    const sf::Vector2f GetCenter() const;

    void SetExtents( sf::Vector2f extents );
    void SetCenter( sf::Vector2f center );

    const sf::Vector2f GetPosition() const override;

  private:
    sf::Vector2f _center;
    sf::Vector2f _halfWidthHeight;
  };
}

#endif // __GAME_AABBCOLLIDER