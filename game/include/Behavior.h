#ifndef __GAME_BEHAVIOR
#define __GAME_BEHAVIOR

#include <functional>
#include <string>

#include <SFML/Window/Event.hpp>

#include "Component.h"

namespace game
{
  struct BehaviorParameters : ComponentParameters
  {
  };

  class Behavior : public Component
  {
  public:
    Behavior( std::string subtype, System& system, ComponentId id ) : Component( subtype, system, id ) {};
    virtual ~Behavior() {};

    virtual void Start() = 0;
    virtual void Update( float deltaTime ) = 0;
  };
}

#endif // __GAME_BEHAVIOR