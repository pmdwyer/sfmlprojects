#ifndef __GAME_BEHAVIORSYSTEM
#define __GAME_BEHAVIORSYSTEM

#include <functional>
#include <memory>
#include <vector>

#include "Behavior.h"
#include "System.h"

namespace game
{
  class BehaviorSystem : public System
  {
  public:
    BehaviorSystem();
    ~BehaviorSystem();

    void Start();
    void Update( float deltaTime );

    void Create( const std::string& type, std::shared_ptr< Entity > parent, const ComponentParameters& params ) override;
    void Remove( ComponentId id ) override;

  private:
    std::vector< std::shared_ptr< Behavior > > _behaviors;
  };
}

#endif // __GAME_BEHAVIORSYSTEM