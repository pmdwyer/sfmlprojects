#ifndef __GAME_SYSTEM
#define __GAME_SYSTEM

#include <string>
#include <memory>

#include "Entity.h"

namespace game
{
  typedef unsigned int ComponentId;

  struct ComponentParameters {};

  class System
  {
  public:
    // Maybe return an error code
    virtual void Create( const std::string& type, std::shared_ptr< Entity > parent, const ComponentParameters& params ) = 0;
    virtual void Remove( ComponentId cid ) = 0;
  };
}

#endif // __GAME_SYSTEM