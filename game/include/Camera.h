#ifndef __GAME_CAMERA
#define __GAME_CAMERA

#include <SFML/Graphics/View.hpp>

namespace game
{
  class Camera
  {
  public:
    Camera();
    ~Camera();

    sf::View GetView();

  private:
    sf::View _view;
  };
}

#endif // __GAME_CAMERA