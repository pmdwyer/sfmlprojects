#ifndef __GAME_GRAPHICCOMPONENT
#define __GAME_GRAPHICCOMPONENT

#include "Component.h"

namespace game
{
  class GraphicComponent : public Component
  {
  public:
    GraphicComponent( std::string type, System& system, ComponentId id ) : Component( type, system, id ) {};
    virtual ~GraphicComponent() {};

    virtual void Draw( sf::RenderTarget& target ) = 0;
  };
}

#endif
