#ifndef __GAME_RIGIDBODY
#define __GAME_RIGIDBODY

#include <cassert>
#include <cmath>
#include <iostream>
#include <limits>
#include <memory>
#include <vector>

#include <SFML/System/Vector2.hpp>

#include "Component.h"
#include "Collider.h"
#include "AABBCollider.h"
#include "CircleCollider.h"

namespace game
{

  enum class RigidBodyType
  {
      PhysicsBody,
      GameBody
  };

  enum class ColliderShape
  {
    None,
    AABB,
    Circle
  };

  struct RigidBodyParameters : public ComponentParameters
  {
    RigidBodyParameters()
      : mass( 1.0f ), friction( 1.0f ), damping( 0.2f ), type( RigidBodyType::PhysicsBody ),
        shape( ColliderShape::None ), colliderParams( nullptr )
    {};
    float mass;
    float friction;
    float damping;
    RigidBodyType type;
    ColliderShape shape;
    std::shared_ptr< ColliderParameters > colliderParams;
  };

  class RigidBody : public Component
  {
  public:
    RigidBody( System& system, ComponentId id );
    ~RigidBody();

    void Update( float dt );
    void AddImpulseForce( const sf::Vector2< float >& force );

    void SetMass( float mass );
    void SetFriction( float friction );
    void SetDamping( float damp );
    void SetBodyType( RigidBodyType rbType );
    void SetColliderShape( ColliderShape shape, const ColliderParameters& params );

    sf::Vector2f GetVelocity() const;
    sf::Vector2f GetAcceleration() const;
    sf::Transformable GetTransformable() const;
    float GetInvMass() const;
    const RigidBodyType GetBodyType() const;
    ColliderShape GetShape() const;
    const Collider& GetCollider() const;

  private:
    RigidBodyType _rbType;
    ColliderShape _shape;

    float _friction;
    float _mass;
    float _invMass;
    float _damping;

    sf::Vector2f _acceleration;
    sf::Vector2f _velocity;
    sf::Vector2f _forceAccum;

    std::shared_ptr< Collider > _collider;

    static float EPSILON;
  };
}

#endif // __GAME_RIGIDBODY