#ifndef __GAME_GRAPHICSSYSTEM
#define __GAME_GRAPHICSSYSTEM

#include <memory>
#include <vector>

#include <SFML/Graphics.hpp>

#include "System.h"
#include "GraphicComponent.h"
#include "BoxGraphic.h"
#include "CircleGraphic.h"
#include "Camera.h"

namespace game
{
  class GraphicsSystem : public System
  {
  public:
    GraphicsSystem();
    ~GraphicsSystem();

    void Render( sf::RenderTarget& target );

    void Create( const std::string& type, std::shared_ptr< Entity > parent, const ComponentParameters& params ) override;
    void Remove( ComponentId cid ) override;

  private:
    std::vector<std::shared_ptr< GraphicComponent > > _graphicComponents;

    Camera _mainCamera;
  };
}

#endif // __GAME_GRAPHICSSYSTEM