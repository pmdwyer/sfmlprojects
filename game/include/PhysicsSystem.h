#ifndef __GAME_PHYSICSSYSTEM
#define __GAME_PHYSICSSYSTEM

#include <memory>
#include <vector>

#include "Entity.h"
#include "System.h"
#include "RigidBody.h"
#include "CollisionUtil.h"

namespace game
{
  class PhysicsSystem : public System
  {
  public:
    PhysicsSystem();
    ~PhysicsSystem();

    void Step( float time );

    void Create( const std::string& type, std::shared_ptr< Entity > parent, const ComponentParameters& params ) override;
    void Remove( ComponentId cid ) override;

  private:
    std::vector< std::shared_ptr< RigidBody > > _rigidBodies;

    static bool Collides( const RigidBody& a, const RigidBody& b );
    static bool Collides( const CircleCollider& a, const CircleCollider& b );
    static bool Collides( const CircleCollider& a, const AABBCollider& b );
    static bool Collides( const AABBCollider& a, const AABBCollider& b );

    static void Resolve( RigidBody& a, RigidBody& b );
  };
}

#endif // __GAME_PHYSICSSYSTEM