#ifndef __GAME_COMPONENT
#define __GAME_COMPONENT

#include <memory>
#include <string>

#include "Entity.h"
#include "System.h"

namespace game
{
  class Component
  {
  public:
    Component( std::string t, System& system, ComponentId id ) 
      : _managingSystem( system ), _id( id ) { SetType( t ); };

    virtual ~Component() = 0;

    virtual void Delete() { _managingSystem.Remove( _id ); };

    std::string GetType() { return _type; };
    void SetType( std::string n ) { _type = n; };

    void SetParent( std::shared_ptr< Entity > parent ) { _parent = parent; };

  protected:
    std::string _type;					        // type of component
    std::shared_ptr< Entity > _parent;	// owner of the component
    System& _managingSystem;            // component manager
    ComponentId _id;                    // component's id in the manager
  };
}

#endif // __GAME_COMPONENT