#ifndef __GAME_CIRCLEGRAPHIC
#define __GAME_CIRCLEGRAPHIC

#include <iostream>

#include <SFML/Graphics/CircleShape.hpp>

#include "GraphicComponent.h"

namespace game
{
  struct CircleGraphicParameters : public ComponentParameters
  {
    float radius;
    sf::Color color;
  };

  class CircleGraphic : public GraphicComponent
  {
  public:
    CircleGraphic( System& system, ComponentId id );
    ~CircleGraphic();

    void SetSize( float radius );
    float GetSize();

    void SetColor( const sf::Color color );
    sf::Color GetColor();

    void Draw( sf::RenderTarget& target ) override;

  private:
    sf::CircleShape _circle;
    sf::Vector2f _halfRadius;
  };
}

#endif // __GAME_CIRCLEGRAPHIC