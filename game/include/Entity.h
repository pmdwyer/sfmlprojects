#ifndef __GAME_ENTITY
#define __GAME_ENTITY

#include <memory>
#include <vector>

#include <SFML/Graphics.hpp>

namespace game
{
  class Scene;

  class Component;

  typedef unsigned long EntityId;

  class Entity
  {
  public:
    Entity( Scene& s, EntityId id, std::string tag );
    ~Entity();

    void AddChild( std::shared_ptr< Entity > child );
    void RemoveChild( std::string tag );
    std::shared_ptr< Entity > GetChild( std::string tag );

    std::shared_ptr< Component > GetComponent( std::string type );
    void AddComponent( std::shared_ptr< Component > comp );

    std::string GetTag() { return _tag; };
    void SetTag( std::string t ) { _tag = t; };

    void Destroy();

    sf::Transformable CalcTransform();

  private:
    EntityId _id;
    std::string _tag;
    Entity* _parent;
    Scene& _owningScene;

    sf::Transform _transform;

    std::vector< std::shared_ptr< Entity > > _children;
    std::vector< std::shared_ptr< Component > > _components;
  };
}

#endif // __GAME_ENTITY