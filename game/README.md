# Goals

## Scene Management
### Remove entities from scenes
  * ~~Add delete to entity~~
  * ~~Add remove to scene~~
  * ~~Add delete to component~~
  * Add entity parenting / childing

### Add Scene transitions
  * reference between scenes
  * must be able to reference scene manager

### Put Scene description in xml files
  * add xml library (yml?)
  * make scenes read from files
    * make entity factories
      * create entity parser
    * make component factories
      * create component parser
    * create scene parser
  * make scene manage load initial scene from name

## Physics system
  * ~~Remove colliders as components~~
  * ~~Add collider to rigid body~~
  * ~~Make AABB collider~~
  * Fix trigger system

## Graphics System
  * ~~Add rectangle graphic~~

## Behavior System
  * Behaviors should have closures

## Add testing framework
  * link boost test ( or other framework )

## Add custom math library
  * matrices
  * matrix stack
    * 3 x 3
    * 4 x 4
  * vectors
  * points
  * quaternions
  * rectangles

## Add custom gui rendering
  * different perspective ( orthographic )
  * post game logic update

## Add Lua scripting
  * link in lua library
  * make behavior system load from files
    * syntax check files
  * add callbacks into engine from lua
    * need to be able to reference and change components
    * need to be able to find other entities in the scene
    * need to be able to load and delete other entities / components
    * need to be able to load other scenes
