# MUST GET DONE FOR FINAL GAME
* snake
  * make head different from body
* scene
  * snake scene
    * reset
  * game over screen
    * high scores
* input
  * record joystick events
* audio
  * eat food sound / grow sound
  * self hit sound
  * death sound

# NICE TO HAVE GARBAGE
* game
  * in game units
  * move window to separate class
    * handle window events
      * focus
      * resize
  * add view / camera
* scene manager
  * ui draw
  * remove hardcoded scene
  * load scene from file
* scene
  * consider re-init method
  * generic scene class
  * load from json
* entity
  * load from json
  * find entity system for other entities?
* map
  * load from json
    * width, height
    * tilemap texture
  * tiles
    * blit texture
  * static map coordinates to pixel coordinates?
* snake
  * load from json
* snake controller
  * don't allow wrapping
* input
  * test for any key press
  * add more joysticks
  * record mouse wheel events
  * consider empty meta keycodes to allow multi single key keycombos
* action map
  * read from file
* math
* texture manager