#!/bin/sh

usage() {
  echo "./build.sh [-f(resh)]"
}

if [[ $1 == '-h' || $1 == '--help' ]]; then
  usage
  exit
fi

BUILD_DIR='build'

if [[ $1 == '-f' ]]; then
  rm -rf $BUILD_DIR
fi

if [[ ! -d ${BUILD_DIR} ]]; then
  mkdir $BUILD_DIR
fi

pushd $BUILD_DIR
cmake -DCMAKE_TOOLCHAIN_FILE=C:/vcpkg/scripts/buildsystems/vcpkg.cmake -G "Visual Studio 16 2019" -A x64 ..
cmake --build .
popd