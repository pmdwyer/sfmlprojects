#ifndef _PMD_SNAKE
#define _PMD_SNAKE

#include <algorithm>
#include <array>
#include <cassert>
#include <iostream>
#include <iterator>

#include <SFML/Graphics.hpp>

#include "Actions.h"
#include "Entity.h"
#include "Maths.h"
#include "IsoMap.h"

using namespace std;

namespace pmd
{
  class Snake : public Entity
  {
    public:
      static constexpr int    MaxLength       = 100;
      static constexpr double MoveTime        = 0.15;
      static constexpr double MaxImmuneTme    = 3.0;
      static constexpr float  Radius          = 4.0f;
      static constexpr float  SegementCenter  = Radius / 2.0;

      using Points = std::array<sf::Vector2i, MaxLength>;
      using Pointsf = std::array<sf::Vector2f, MaxLength>;
      using Circles = std::array<sf::CircleShape, MaxLength>;

      enum class State
      {
        Stationary,
        Moving,
        Eating
      };

      Snake() = default;

      bool init(IsoMap* m)
      {
        map = m;
        
        lives         = 3;
        immune        = false;
        immuneTime    = 0.0;
        dt            = 0.0;
        currentState  = State::Stationary;
        currentDirection = Action::MoveNorthEast;

        currentLength = 3;
        positions[0] = {5, 5};
        positions[1] = {5, 6};
        positions[2] = {5, 7};

        pixelPositions[0] = map->mapCoordsToPixel(positions[0]);
        pixelPositions[1] = map->mapCoordsToPixel(positions[1]);
        pixelPositions[2] = map->mapCoordsToPixel(positions[2]);

        setupSegement(0);
        setupSegement(1);
        setupSegement(2);

        return true;
      };

      void update(double deltaTime)
      {
        this->dt += deltaTime;
        if (immune)
        {
          immuneTime += deltaTime;
          if (immuneTime >= MaxImmuneTme)
          {
            immune = false;
            immuneTime = 0.0;
          }
        }

        if (currentState != Snake::State::Moving)
        {
          sf::Vector2i offset = {0, 0};
          switch (currentDirection)
          {
            case Action::MoveNorthEast:
              offset += {-1, 0};
              break;
            case Action::MoveSouthEast:
              offset += {0, 1};
              break;
            case Action::MoveNorthWest:
              offset += {0, -1};
              break;
            case Action::MoveSouthWest:
              offset += {1, 0};
              break;
            default:
              assert(false);
              break;
          }

          sf::Vector2i pos = positions[0] + offset;
          nextPosition.x = (pos.x + map->width) % map->width;
          nextPosition.y = (pos.y + map->height) % map->height;
          nextPixelPosition = map->mapCoordsToPixel(nextPosition);
          currentState = Snake::State::Moving;
        }
        else
        {
          if (this->dt >= MoveTime)
          {
            this->dt = 0.0;
            currentState = Snake::State::Stationary;
            updatePositions(nextPosition, nextPixelPosition);
          }
        }
      };

      void draw(sf::RenderTarget& target)
      {
        double percent = dt / MoveTime;

        sf::Vector2f pos = pixelPositions[0];
        sf::Vector2f nextPos = nextPixelPosition;
        sf::Vector2f drawPos;
        sf::RenderStates states;

        drawPos.x = (float) lerp(pos.x, nextPos.x, percent);
        drawPos.y = (float) lerp(pos.y, nextPos.y, percent);
        states.transform.translate(drawPos);
        target.draw(segements[0], states);

        for (int i = 1; i < currentLength; i++)
        {
          states = sf::RenderStates::Default;
          pos = pixelPositions[i];
          nextPos = pixelPositions[i - 1];

          drawPos.x = (float) lerp(pos.x, nextPos.x, percent);
          drawPos.y = (float) lerp(pos.y, nextPos.y, percent);
          states.transform.translate(drawPos);
          target.draw(segements[i], states);
        }
      };

      void addSegment(sf::Vector2i tailPos)
      {
        assert(currentLength < MaxLength);
        sf::Vector2f tailPixelPos = map->mapCoordsToPixel(tailPos);
        positions[currentLength] = tailPos;
        pixelPositions[currentLength] = tailPixelPos;
        setupSegement(currentLength);
        currentLength++;
      };

      void setupSegement(int idx)
      {
        segements[idx].setRadius(Radius);
        segements[idx].setOrigin(SegementCenter, SegementCenter);
        segements[idx].setFillColor(sf::Color::Green);
      };

      void updatePositions(sf::Vector2i nextPoint, sf::Vector2f nextPixelPos)
      {
        for (int i = MaxLength - 1; i > 0; i--)
        {
          positions[i] = positions[i - 1];
          pixelPositions[i] = pixelPositions[i - 1];
        }
        positions[0] = nextPoint;
        pixelPositions[0] = nextPixelPos;
      };

      bool collidesWith(const sf::Vector2i& point)
      {
        return any_of(begin(positions), begin(positions) + currentLength, 
          [&](const sf::Vector2i& segmentPoint){ return segmentPoint == point; });
      };

      bool selfCollision()
      {
        if (immune)
          return false;

        return currentState == State::Moving && collidesWith(nextPosition);
      };

      void doHit()
      {
        immune = true;
        lives--;
      };

      int           lives         = 3;
      bool          immune        = false;
      double        immuneTime    = 0.0;
      int           currentLength = 3;
      double        dt            = 0.0;
      State         currentState  = State::Stationary;
      Action        currentDirection = Action::MoveNorthEast;

      sf::Vector2i nextPosition;
      sf::Vector2f nextPixelPosition;
      Points positions;
      Pointsf pixelPositions;
      Circles segements;
      IsoMap *map;
  };
}

#endif // _PMD_SNAKE