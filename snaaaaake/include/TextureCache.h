#ifndef _PMD_TEXTURECACHE
#define _PMD_TEXTURECACHE

#include <unordered_map>

#include <SFML/Graphics.hpp>

using namespace std;

namespace pmd
{
  class TextureCache
  {
    public:
      static TextureCache& getInstance()
      {
        static TextureCache cache;
        return cache;
      }

      sf::Texture& getTexture(const string& fname)
      {
        auto t = m_textures.find(fname);
        if (t == m_textures.end())
          return loadTexture(fname);

        return t->second;
      };

      sf::Texture& loadTexture(const string& fname)
      {
        sf::Texture t;
        t.loadFromFile("../../assets/textures/" + fname);
        m_textures[fname] = t;
        return m_textures[fname];
      }

      TextureCache(const TextureCache&) = delete;
      TextureCache(TextureCache&&) = delete;
      TextureCache& operator=(const TextureCache&) = delete;
      TextureCache& operator=(TextureCache&&) = delete;

    private:
      TextureCache() = default;

      unordered_map<string, sf::Texture>  m_textures;
  };
}

#endif // _PMD_TEXTURECACHE