#ifndef _PMD_SNAKESCENE
#define _PMD_SNAKESCENE

#include <iostream>

#include "Scene.h"
#include "SceneManager.h"
#include "Snake.h"
#include "IsoMap.h"
#include "SnakeController.h"
#include "Actions.h"
#include "Food.h"
#include "PauseScene.h"
#include "EndScene.h"
#include "Font.h"
#include "FontCache.h"

namespace pmd
{
  class SnakeScene : public Scene<SnakeScene>
  {
    public:
      explicit SnakeScene(SceneManager *mgr) : Scene<SnakeScene>{mgr} {};
      ~SnakeScene() = default;

      bool init() override
      {
        bool res = true;
        res &= m_map.init();
        res &= m_food.init(&m_map);
        res &= m_snake.init(&m_map);
        res &= m_snakeControl.init(&m_snake, &m_map);

        Font& hack = FontCache::getInstance().getFont("HackRegular");
        m_lives.setCharacterSize(16);
        m_lives.setFillColor(sf::Color::Yellow);
        m_lives.setFont(hack.font);
        m_lives.setPosition({(float)1280 * 0.9f, (float) 720 * 0.1f});
        m_lives.setString("Lives: " + std::to_string(m_snake.lives));

        m_length.setCharacterSize(16);
        m_length.setFillColor(sf::Color::Yellow);
        m_length.setFont(hack.font);
        m_length.setPosition({(float)1280 * 0.9f, (float) 720 * 0.15f});
        m_length.setString("Length: " + std::to_string(m_snake.currentLength));
        return res;
      };

      void handleInput(const InputManager& input) override
      {
        if (input.isPressed(KeyCode::Esc))
        {
          manager->push<PauseScene>();
        }
        
        m_snakeControl.handleInput(input);
      };

      void update(double dt) override
      {
        m_snake.update(dt);
        if (m_snake.collidesWith(m_food.location))
        {
          sf::Vector2i newTail = m_snake.positions[m_snake.currentLength];
          m_snake.addSegment(newTail);
          m_length.setString("Length: " + std::to_string(m_snake.currentLength));
          m_food.spawn();
        }
        else if (m_snake.selfCollision())
        {
          m_snake.doHit();
          m_lives.setString("Lives: " + std::to_string(m_snake.lives));
          if (m_snake.lives <= 0)
          {
            manager->pop();
            manager->push<EndScene>();
          }
        }
      }

      void draw(sf::RenderTarget& target) override
      {
        m_map.draw(target);
        m_food.draw(target);
        m_snake.draw(target);
        target.draw(m_lives);
        target.draw(m_length);
      };

    private:
      enum State
      {
        Playing,
        Paused
      };

      SnakeController m_snakeControl;
      Snake m_snake;
      IsoMap m_map;
      Food m_food;
      State state = State::Playing;
      sf::Text m_lives;
      sf::Text m_length;
  };
}

#endif // _PMD_SNAKESCENE