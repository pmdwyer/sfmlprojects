#ifndef _PMD_INPUTS
#define _PMD_INPUTS

#include <set>

#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Window/Joystick.hpp>

namespace pmd
{
  enum ButtonState
  {
    Released,
    Pressed
  };

  enum KeyCode
  {
    A = sf::Keyboard::Key::A,
    B = sf::Keyboard::Key::B,
    C = sf::Keyboard::Key::C,
    D = sf::Keyboard::Key::D,
    E = sf::Keyboard::Key::E,
    F = sf::Keyboard::Key::F,
    G = sf::Keyboard::Key::G,
    H = sf::Keyboard::Key::H,
    I = sf::Keyboard::Key::I,
    J = sf::Keyboard::Key::J,
    K = sf::Keyboard::Key::K,
    L = sf::Keyboard::Key::L,
    M = sf::Keyboard::Key::M,
    N = sf::Keyboard::Key::N,
    O = sf::Keyboard::Key::O,
    P = sf::Keyboard::Key::P,
    Q = sf::Keyboard::Key::Q,
    R = sf::Keyboard::Key::R,
    S = sf::Keyboard::Key::S,
    T = sf::Keyboard::Key::T,
    U = sf::Keyboard::Key::U,
    V = sf::Keyboard::Key::V,
    W = sf::Keyboard::Key::W,
    X = sf::Keyboard::Key::X,
    Y = sf::Keyboard::Key::Y,
    Z = sf::Keyboard::Key::Z,
    Zero = sf::Keyboard::Key::Num0,
    One = sf::Keyboard::Key::Num1,
    Two = sf::Keyboard::Key::Num2,
    Three = sf::Keyboard::Key::Num3,
    Four = sf::Keyboard::Key::Num4,
    Five = sf::Keyboard::Key::Num5,
    Six = sf::Keyboard::Key::Num6,
    Seven = sf::Keyboard::Key::Num7,
    Eight = sf::Keyboard::Key::Num8,
    Nine = sf::Keyboard::Key::Num9,
    Esc = sf::Keyboard::Key::Escape,
    LCtrl = sf::Keyboard::Key::LControl,
    LShift = sf::Keyboard::Key::LShift,
    LAlt = sf::Keyboard::Key::LAlt,
    LGui = sf::Keyboard::Key::LSystem,
    RCtrl = sf::Keyboard::Key::RControl,
    RShift = sf::Keyboard::Key::RShift,
    RAlt = sf::Keyboard::Key::RAlt,
    RGui = sf::Keyboard::Key::RSystem,
    Menu = sf::Keyboard::Key::Menu,
    LBracket = sf::Keyboard::Key::LBracket,
    RBracket = sf::Keyboard::Key::RBracket,
    Semicolon = sf::Keyboard::Key::SemiColon,
    Comma = sf::Keyboard::Key::Comma,
    Period = sf::Keyboard::Key::Period,
    Quote = sf::Keyboard::Key::Quote,
    Slash = sf::Keyboard::Key::Slash,
    Backslash = sf::Keyboard::Key::BackSlash,
    Grave = sf::Keyboard::Key::Tilde,
    Equal = sf::Keyboard::Key::Equal,
    Hyphen = sf::Keyboard::Key::Dash,
    Space = sf::Keyboard::Key::Space,
    Enter = sf::Keyboard::Key::Return,
    Backspace = sf::Keyboard::Key::BackSpace,
    Tab = sf::Keyboard::Key::Tab,
    PageUp = sf::Keyboard::Key::PageUp,
    PageDown = sf::Keyboard::Key::PageDown,
    Home = sf::Keyboard::Key::Home,
    End = sf::Keyboard::Key::End,
    Insert = sf::Keyboard::Key::Insert,
    Delete = sf::Keyboard::Key::Delete,
    NumpadPlus = sf::Keyboard::Key::Add,
    NumpadMinus = sf::Keyboard::Key::Subtract,
    NumpadAsterisk = sf::Keyboard::Key::Multiply,
    NumpadSlash = sf::Keyboard::Key::Divide,
    Left = sf::Keyboard::Key::Left,
    Right = sf::Keyboard::Key::Right,
    Up = sf::Keyboard::Key::Up,
    Down = sf::Keyboard::Key::Down,
    NumpadZero = sf::Keyboard::Key::Numpad0,
    NumpadOne = sf::Keyboard::Key::Numpad1,
    NumpadTwo = sf::Keyboard::Key::Numpad2,
    NumpadThree = sf::Keyboard::Key::Numpad3,
    NumpadFour = sf::Keyboard::Key::Numpad4,
    NumpadFive = sf::Keyboard::Key::Numpad5,
    NumpadSix = sf::Keyboard::Key::Numpad6,
    NumpadSeven = sf::Keyboard::Key::Numpad7,
    NumpadEight = sf::Keyboard::Key::Numpad8,
    NumpadNine = sf::Keyboard::Key::Numpad9,
    F1 = sf::Keyboard::Key::F1,
    F2 = sf::Keyboard::Key::F2,
    F3 = sf::Keyboard::Key::F3,
    F4 = sf::Keyboard::Key::F4,
    F5 = sf::Keyboard::Key::F5,
    F6 = sf::Keyboard::Key::F6,
    F7 = sf::Keyboard::Key::F7,
    F8 = sf::Keyboard::Key::F8,
    F9 = sf::Keyboard::Key::F9,
    F10 = sf::Keyboard::Key::F10,
    F11 = sf::Keyboard::Key::F11,
    F12 = sf::Keyboard::Key::F12,
    F13 = sf::Keyboard::Key::F13,
    F14 = sf::Keyboard::Key::F14,
    F15 = sf::Keyboard::Key::F15,
    Pause = sf::Keyboard::Key::Pause,

    // keys not represented in sfml
    CapsLock,
    ScrollLock,
    PrintScreen,
    NumLock,
    NumpadEnter,
    NumpadPeriod,
    MaxKeys
  };

  enum MouseWheel
  {
    WheelUp,
    WheelDown
  };

  struct keyboard
  {
    static constexpr int NumKeys = 104;
    ButtonState keys[NumKeys];
  };

  struct mouse
  {
    static constexpr int NumButtons = 10;
    ButtonState   buttons[NumButtons];
    MouseWheel    wheel;
    sf::Vector2i  position;
  };

  struct joystick
  {
    static constexpr int NumButtons = 12;
    ButtonState   buttons[NumButtons];
    float         ltrigger;
    float         rtrigger;
    sf::Vector2f  lstick;
    sf::Vector2f  rstick;
  };
  
  using Keyboard = struct keyboard;
  using Mouse = struct mouse;
  using Joystick = struct joystick;
  using KeyCombo = std::set<KeyCode>;
}

#endif // _PMD_INPUTS