#ifndef _PMD_SNAKECONTROLLER
#define _PMD_SNAKECONTROLLER

#include <iostream>

#include "Snake.h"
#include "IsoMap.h"
#include "InputManager.h"
#include "ActionMap.h"

namespace pmd
{
  class SnakeController
  {
    public:
      SnakeController()
        : m_actions{}
      {};

      bool init(Snake *snake, IsoMap *map)
      {
        m_snake = snake;
        m_map = map;

        // setup inputs to actions
        m_actions.keymap[{KeyCode::Q}] = Action::MoveNorthWest;
        m_actions.keymap[{KeyCode::E}] = Action::MoveNorthEast;
        m_actions.keymap[{KeyCode::A}] = Action::MoveSouthWest;
        m_actions.keymap[{KeyCode::D}] = Action::MoveSouthEast;

        return true;
      };

      void handleInput(const InputManager& input)
      {
        m_actions.process(input);
        if (!m_actions.anyInput())
          return;

        if (m_actions.isActive(Action::MoveNorthEast) && m_snake->currentDirection != Action::MoveSouthWest)
          m_snake->currentDirection = Action::MoveNorthEast;
        else if (m_actions.isActive(Action::MoveSouthEast) && m_snake->currentDirection != Action::MoveNorthWest)
          m_snake->currentDirection = Action::MoveSouthEast;
        else if (m_actions.isActive(Action::MoveNorthWest) && m_snake->currentDirection != Action::MoveSouthEast)
          m_snake->currentDirection = Action::MoveNorthWest;
        else if (m_actions.isActive(Action::MoveSouthWest) && m_snake->currentDirection != Action::MoveNorthEast)
          m_snake->currentDirection = Action::MoveSouthWest;
      };

      Snake *m_snake;
      IsoMap *m_map;
      ActionMap m_actions;
  };
}

#endif // _PMD_SNAKECONTROLLER