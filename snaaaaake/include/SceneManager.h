#ifndef _PMD_SCENEMANAGER
#define _PMD_SCENEMANAGER

#include <cassert>
#include <iostream>
#include <type_traits>
#include <unordered_map>
#include <vector>

#include "Scene.h"
#include "InputManager.h"

using namespace std;

namespace pmd
{
  class SceneManager
  {
    public:
      SceneManager(sf::Window *window) : m_scenes{}, m_window{window} {};

      ~SceneManager()
      {
        for (auto& scene : m_loadedScenes)
          if (scene.second)
            delete scene.second;
      }

      template <typename S>
      void push()
      {
        static_assert(std::is_base_of_v<Scene<S>, S>, "Must derive from Scene");

        S *scene;
        if (m_loadedScenes.find(S::id) != m_loadedScenes.end())
        {
          scene = dynamic_cast<S *>(m_loadedScenes[S::id]);
        }
        else
        {
          scene = new S(this);
          m_loadedScenes[S::id] = scene;
        }
        scene->init();
        m_scenes.push_back(scene);
      };

      void pop()
      {
        assert(m_scenes.size() > 0);
        BaseScene *scene = m_scenes[m_scenes.size() - 1];
        m_scenes.pop_back();
      };

      void handleInput(const InputManager& input)
      {
        assert(m_scenes.size() > 0);
        m_scenes[m_scenes.size() - 1]->handleInput(input);
      };

      void update(double dt)
      {
        assert(dt >= 0.0);
        assert(m_scenes.size() > 0);
        m_scenes[m_scenes.size() - 1]->update(dt);
      };

      void draw(sf::RenderTarget& target)
      {
        assert(m_scenes.size() > 0);
        m_scenes[m_scenes.size() - 1]->draw(target);
      };

      sf::Window *getWindow()
      {
        return m_window;
      }

    private:
      vector<BaseScene *> m_scenes;
      unordered_map<SceneId, BaseScene *> m_loadedScenes;
      sf::Window *m_window;
  };
}

#endif // _PMD_SCENEMANAGER