#ifndef _PMD_GAME
#define _PMD_GAME

#include <iostream>
#include <string>

#include <SFML/Graphics.hpp>

#include "SceneManager.h"
#include "MenuScene.h"
#include "InputManager.h"
#include "Font.h"
#include "FontCache.h"

namespace pmd
{
  class Game
  {
    public:
      Game(int width, int height)
        : m_quit{false},
          m_width{width}, m_height{height},
          m_window{},
          m_manager{&m_window},
          m_hackFont{}
      {
      };

      ~Game()
      {
        m_window.close();
      };

      void init()
      {
        m_window.create(sf::VideoMode(m_width, m_height), "snaaaaake");

        FontCache& cache = FontCache::getInstance();
        cache.loadFont("../../assets/fonts/Hack-Regular.ttf", "HackRegular");
        cache.loadFont("../../assets/fonts/Hack-Bold.ttf", "HackBold");
        cache.loadFont("../../assets/fonts/Hack-BoldItalic.ttf", "HackBoldItalic");
        cache.loadFont("../../assets/fonts/Hack-Italic.ttf", "HackItalic");
        m_hackFont = cache.getFont("HackBold");

        m_manager.push<MenuScene>();
      }

      void run()
      {
        m_window.setFramerateLimit(60);

        sf::Clock clock;
        sf::Time dt;
        float fps = 0.0f;
        while (!m_quit && m_window.isOpen())
        {
          dt = clock.getElapsedTime();
          fps = 1.0f / dt.asSeconds();
          clock.restart();

          sf::Event e;
          m_inputManager.beingInput();
          while (m_window.pollEvent(e))
          {
            if (e.type == sf::Event::Closed)
            {
              m_quit = true;
              break;
            }

            m_inputManager.recordEvent(e);
          }
          m_inputManager.endInput();

          m_manager.handleInput(m_inputManager);

          m_manager.update(dt.asSeconds());

          m_window.clear(sf::Color::Black);
          
          m_manager.draw(m_window);
          m_hackFont.draw(m_window, std::to_string(fps), {10.0f, 10.0f}, 12);

          m_window.display();
        }
      };

    private:
      bool m_quit;
      int m_width;
      int m_height;
      sf::RenderWindow m_window;
      SceneManager m_manager;
      InputManager m_inputManager;
      Font m_hackFont;
  };
}

#endif _PMD_GAME