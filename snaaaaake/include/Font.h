#ifndef _PMD_FONT
#define _PMD_FONT

#include <string>

#include <SFML/Graphics.hpp>

namespace pmd
{
  class Font
  {
    public:
      Font() = default;

      bool init(const std::string& path)
      {
        loaded = font.loadFromFile(path);
        return loaded;
      };

      void draw(sf::RenderTarget& target, const std::string& str, const sf::Vector2f& location, int charSize)
      {
        if (!loaded)
          return;

        sf::Text text;
        text.setString(str);
        text.setCharacterSize(charSize);
        text.setFont(font);

        sf::RenderStates states;
        states.transform.translate(location);
        target.draw(text, states);
      };

      sf::Font font;
      bool loaded = false;
  };
}

#endif // _PMD_FONT