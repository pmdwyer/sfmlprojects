#ifndef _PMD_MATHS
#define _PMD_MATHS

#include <cmath>

namespace pmd
{
  inline double lerp(double a, double b, double percent)
  {
    return (1 - percent) * a + (percent * b);
  }

  template<typename T>
  inline T clampLower(T num, T lowerBound)
  {
    static_assert(std::is_arithmetic_v<T>, "Only numeric types can be clamped.");
    return num < lowerBound ? lowerBound : num;
  }

  template<typename T>
  inline T clampUpper(T num, T upperBound)
  {
    static_assert(std::is_arithmetic_v<T>, "Only numeric types can be clamped.");
    return num > upperBound ? upperBound : num;
  }

  template<typename T>
  inline T clamp(T num, T lowerBound, T upperBound)
  {
    static_assert(std::is_arithmetic_v<T>, "Only numeric types can be clamped.");
    T ret = num < lowerBound ? lowerBound : num;
    return ret > upperBound ? upperBound : ret;
  }
}

#endif // _PMD_MATHS