#ifndef _PMD_FOOD
#define _PMD_FOOD

#include <random>

#include <SFML/Graphics.hpp>

#include "Entity.h"
#include "IsoMap.h"

using namespace std;

namespace pmd
{
  class Food : public Entity
  {
    public:
      Food() = default;

      bool init(const IsoMap *m)
      {
        map = m;
        random_device device;
        generator.seed(device());
        food.setFillColor(sf::Color::Blue);
        food.setOutlineColor(sf::Color::Black);
        food.setOrigin(3.0f * (float)sqrt(2.0), 3.0f * (float)sqrt(2.0));
        food.setRadius(6.0f);
        spawn();
        return true;
      };

      void draw(sf::RenderTarget& target)
      {
        sf::RenderStates states;
        states.transform.translate(pixelLocation);
        target.draw(food, states);
      };

      void spawn()
      {
        location.x = map->widthDist(generator);
        location.y = map->heightDist(generator);
        pixelLocation = map->mapCoordsToPixel(location);
      };

      sf::CircleShape food;
      sf::Vector2i    location;
      sf::Vector2f    pixelLocation;

      const IsoMap *map;

      mt19937 generator;
  };
}

#endif // _PMD_FOOD