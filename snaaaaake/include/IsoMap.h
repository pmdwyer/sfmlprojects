#ifndef _PMD_ISOMAP
#define _PMD_ISOMAP

#include <cassert>
#include <memory>
#include <random>

#include <SFML/Graphics.hpp>

#include "Tile.h"

using namespace std;

namespace pmd
{
  class IsoMap
  {
    public:
      IsoMap()
        : widthDist(0, width - 1),
          heightDist(0, height - 1)
      {
        tiles = new Tile[height * width];
      };

      ~IsoMap()
      {
        delete [] tiles;
      };

      bool init()
      {
        origin = {610.0f, 36.0f};
        calculateTiles();
        return true;
      }

      void draw(sf::RenderTarget& target)
      {
        for (int i = 0; i < width; i++)
          for (int j = 0; j < height; j++)
          {
            tiles[i + j * width].draw(target);
          }
      }

      void calculateTiles()
      {
        for (int j = 0; j < height; j++)
        {
          for (int i = 0; i < width; i++)
          {
            tiles[i + j * width].configure(i, j, origin);
          }
        }
      }

      sf::Vector2f mapCoordsToPixel(sf::Vector2i pos) const
      {
        assert(pos.x >= 0 && pos.y >= 0);
        return tiles[pos.x + pos.y * width].center;
      }

      int height = 20;
      int width = 20;
      sf::Vector2f origin = {0.0f, 0.0f};
      Tile* tiles;

      uniform_int_distribution<> widthDist;
      uniform_int_distribution<> heightDist;
  };
}

#endif // _PMD_ISOMAP