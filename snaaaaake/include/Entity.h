#ifndef _PMD_ENTITY
#define _PMD_ENTITY

namespace pmd
{
  class Entity
  {
    public:
      Entity() : Id{m_allId++} {};
      ~Entity() = default;
      int Id;

    private:
      static int m_allId;
  };

  int Entity::m_allId = 1;
}

#endif // _PMD_ENTITY