#ifndef _PMD_INPUTMANAGER
#define _PMD_INPUTMANAGER

#include <SFML/Window.hpp>

#include "Inputs.h"

namespace pmd
{
  class InputManager
  {
    public:
      InputManager() = default;

      void init()
      {
        // for adjusting sensitivity of mouse / joysticks
      };

      void beingInput()
      {
        memcpy(&keyboard.keys, &prevKeyboard.keys, sizeof(KeyCode) * Keyboard::NumKeys);
        memset(&prevKeyboard.keys, ButtonState::Released, sizeof(KeyCode) * Keyboard::NumKeys); // this maybe redundant

        // memcpy(&prevMouse, &mouse, sizeof(Mouse));
        // memset(&mouse, 0, sizeof(Mouse));

        // memcpy(&prevJoystick, &joystick, sizeof(Joystick));
        // memset(&joystick, 0, sizeof(Joystick));
      };

      void endInput()
      {
        memcpy(&prevKeyboard, &keyboard, sizeof(Keyboard));
      }

      void recordEvent(const sf::Event& e)
      {
        int idx;
        switch (e.type)
        {
        case sf::Event::KeyPressed:
          idx = e.key.code;
          keyboard.keys[idx] = Pressed;
          break;
        case sf::Event::KeyReleased:
          idx = e.key.code;
          keyboard.keys[idx] = Released;
          break;
        case sf::Event::MouseButtonPressed:
          idx = e.mouseButton.button;
          mouse.buttons[idx] = Pressed;
          break;
        case sf::Event::MouseButtonReleased:
          idx = e.mouseButton.button;
          mouse.buttons[idx] = Released;
          break;
        case sf::Event::MouseMoved:
          mouse.position.x = e.mouseMove.x;
          mouse.position.y = e.mouseMove.y;
          break;
        case sf::Event::MouseWheelScrolled:
          break;
        case sf::Event::JoystickButtonPressed:
        case sf::Event::JoystickButtonReleased:
        case sf::Event::JoystickMoved:
          break;
        };
      }

      inline bool isPressed(KeyCode code) const
      {
        return keyboard.keys[code] == ButtonState::Pressed;
      };

      Keyboard keyboard;
      Keyboard prevKeyboard;

      Mouse mouse;
      Mouse prevMouse;

      Joystick joystick;
      Joystick prevJoystick;
  };
}

#endif // _PMD_INPUTMANAGER