#ifndef _PMD_ENDSCENE
#define _PMD_ENDSCENE

#include <SFML/Graphics.hpp>

#include "SceneManager.h"
#include "Scene.h"
#include "Font.h"
#include "FontCache.h"

namespace pmd
{
  class MenuScene;

  class EndScene : public Scene<EndScene>
  {
    public:
      EndScene(SceneManager *manager) : Scene<EndScene>{manager} {};
      ~EndScene() = default;

      bool init()
      {
        Font& hack = FontCache::getInstance().getFont("HackBoldItalic");
        if (!hack.loaded)
          return false;

        gameOver.setCharacterSize(36);
        gameOver.setFont(hack.font);
        gameOver.setString("Game Over, Man!");
        gameOver.setFillColor(sf::Color::Red);

        sf::FloatRect gameOverBounds = gameOver.getLocalBounds();
        gameOver.setOrigin(gameOverBounds.width / 2.0f, gameOverBounds.height / 2.0f);
        gameOver.setPosition(640, 360 - gameOverBounds.height / 2);
        return true;
      };

      void handleInput(const InputManager& input)
      {
        if ( input.isPressed(KeyCode::Esc)
          || input.isPressed(KeyCode::Enter)
          || input.isPressed(KeyCode::Q) )
        {
          manager->pop();
          manager->push<MenuScene>();
        }
      };

      void update(double dt)
      {
      };

      void draw(sf::RenderTarget& target)
      {
        target.draw(gameOver);
      };

      sf::Text gameOver;
  };
}

#endif // _PMD_ENDSCENE