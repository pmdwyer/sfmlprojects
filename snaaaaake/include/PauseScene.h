#ifndef _PMD_PAUSESCENE
#define _PMD_PAUSESCENE

#include <functional>
#include <iostream>
#include <vector>

#include <SFML/Graphics.hpp>

#include "Maths.h"
#include "Scene.h"
#include "SceneManager.h"
#include "FontCache.h"

using namespace std;

namespace pmd
{
  class PauseScene : public Scene<PauseScene>
  {
    public:
      explicit PauseScene(SceneManager *mgr) : Scene<PauseScene>{mgr} {};
      ~PauseScene()
      {
        for (auto item : menuItems)
          if (item)
            delete item;
      }

      bool init()
      {
        Font& hack = FontCache::getInstance().getFont("HackRegular");
        if (!hack.loaded)
          return false;

        sf::Text *play = new sf::Text;
        play->setCharacterSize(24);
        play->setFillColor(sf::Color::Cyan);
        play->setOutlineColor(sf::Color::Magenta);
        play->setFont(hack.font);
        play->setString("Play");
        sf::FloatRect playBounds = play->getLocalBounds();
        play->setOrigin(playBounds.width / 2.0f, playBounds.height / 2.0f);
        play->move(640, 360 - playBounds.height);

        sf::Text *quit = new sf::Text;
        quit->setCharacterSize(24);
        quit->setFillColor(sf::Color::Cyan);
        quit->setOutlineColor(sf::Color::Magenta);
        quit->setFont(hack.font);
        quit->setString("Quit");
        sf::FloatRect quitBounds = quit->getLocalBounds();
        quit->setOrigin(quitBounds.width / 2.0f, quitBounds.height / 2.0f);
        quit->move(640, 360 + quitBounds.height);

        menuItems.push_back(play);
        menuItemActions.push_back(
          [&]()
          {
            doSelection = false;
            manager->pop();
          }
        );
        menuItems.push_back(quit);
        menuItemActions.push_back(
          [&]()
          {
            manager->getWindow()->close();
          }
        );

        selection = 0;
        menuItems[selection]->setOutlineThickness(1.0f);

        return true;
      };

      void handleInput(const InputManager& input) override
      {
        int prevSelection = selection;
        if (input.isPressed(KeyCode::Up))
          selection = clampLower<int>(selection - 1, 0);
        if (input.isPressed(KeyCode::Down))
          selection = clampUpper<int>(selection + 1, 1);

        if (input.isPressed(KeyCode::Enter))
          doSelection = true;

        menuItems[prevSelection]->setOutlineThickness(0.0f);
        menuItems[selection]->setOutlineThickness(1.0f);
      };

      void update(double dt) override
      {
        if (doSelection)
          menuItemActions[selection]();
      };

      void draw(sf::RenderTarget& target) override
      {
        for (int i = 0; i < menuItems.size(); i++)
        {
          target.draw(*(menuItems[i]));
        }
      };

      int selection = 0;
      vector<sf::Text *> menuItems;
      vector<function<void()>> menuItemActions;
      bool doSelection = false;
  };
}

#endif // _PMD_PAUSESCENE