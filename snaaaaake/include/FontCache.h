#ifndef _PMD_FONTCACHE
#define _PMD_FONTCACHE

#include <unordered_map>

#include "Font.h"

namespace pmd
{
  class FontCache
  {
    public:
      static FontCache& getInstance()
      {
        static FontCache instance;
        return instance;
      };

      void loadFont(const std::string& path, const std::string& name)
      {
        Font f;
        f.init(path);
        m_fonts[name] = f;
      };

      Font& getFont(const std::string& name)
      {
        return m_fonts[name];
      };

      FontCache(const FontCache&) = delete;
      FontCache(FontCache&&) = delete;
      FontCache& operator=(const FontCache&) = delete;
      FontCache& operator=(FontCache&&) = delete;
    
    private:
      FontCache() = default;

      std::unordered_map<std::string, Font> m_fonts;
  };
}

#endif // _PMD_FONTCACHE