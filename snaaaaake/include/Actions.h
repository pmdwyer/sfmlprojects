#ifndef _PMD_ACTIONS
#define _PMD_ACTIONS

#include <SFML/Graphics.hpp>

namespace pmd
{
  enum Action
  {
    // Movement
    MoveNorth,
    MoveNorthEast,
    MoveEast,
    MoveSouthEast,
    MoveSouth,
    MoveSouthWest,
    MoveWest,
    MoveNorthWest,

    // Menu
    MenuOpen,
    MenuClose,
    MenuUp,
    MenuDown,
    MenuLeft,
    MenuRight,
    MenuActivate,

    MaxActions
  };

  enum ActionState
  {
    Inactive,
    Active
  };
}

#endif // _PMD_ACTIONS