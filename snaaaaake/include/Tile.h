#ifndef _PMD_TILE
#define _PMD_TILE

#include <SFML/Graphics.hpp>

#include "FontCache.h"
#include "TextureCache.h"

namespace pmd
{
  class Tile
  {
    public:
      Tile() : vertices(sf::Quads, 4), centerDot(2.0f), outline(sf::Lines, 8) {};

      void configure(int row, int col, sf::Vector2f offset)
      {
        index = {col, row};

        float pixelx = (-0.5f * row * Tile::Width) + (0.5f * col * Tile::Width) + offset.x;
        float pixely = (0.5f * row * Tile::Height) + (0.5f * col * Tile::Height) + offset.y;
        float cx = sqrt(2.0f);

        center = {pixelx + 0.5f * Tile::Width, pixely + 0.5f * Tile::Height};

        centerDot.setFillColor(sf::Color::Red);
        centerDot.setOrigin(cx, cx);
        centerDot.setPosition(center);

        x = {pixelx, pixely};
        y = {pixelx + Tile::Width, pixely};
        z = {pixelx + Tile::Width, pixely + Tile::Height};
        w = {pixelx, pixely + Tile::Height};

        vertices[0].position = x;
        vertices[1].position = y;
        vertices[2].position = z;
        vertices[3].position = w;

        outline[0].position = x;
        outline[1].position = y;
        outline[2].position = x;
        outline[3].position = w;
        outline[4].position = y;
        outline[5].position = z;
        outline[6].position = w;
        outline[7].position = z;
        
        texture = TextureCache::getInstance().getTexture("tileable-S7002876-grey.png");
        float tx = (float) texture.getSize().x;
        float ty = (float) texture.getSize().y;// * 0.795f;
        vertices[0].texCoords = {0.0f, 0.0f};
        vertices[1].texCoords = {tx, 0.0f};
        vertices[2].texCoords = {tx, ty};
        vertices[3].texCoords = {0.0f, ty};

        Font& hack = FontCache::getInstance().getFont("HackRegular");
        label.setFont(hack.font);
        label.setString("(" + to_string(col) + ", " + to_string(row) + ")");
        label.setCharacterSize(8);
        label.setPosition(center);
      }

      void draw(sf::RenderTarget& target)
      {
        sf::IntRect viewport = target.getViewport(target.getView());
        if (!viewport.contains((int)center.x, (int)center.y))
          return;

        sf::RenderStates states;
        states.texture = &texture;
        // target.draw(centerDot);
        target.draw(vertices, states);
        // target.draw(outline);
        // target.draw(label);
      };

      static constexpr int Width = 64;
      // static constexpr int Height = 64;
      // static constexpr int Width = 32;
      static constexpr int Height = 32;

      sf::Vector2i index  = {0, 0};
      sf::Vector2f center = {0.0f, 0.0f};

      sf::Vector2f x = {0.0f, 0.0f};
      sf::Vector2f y = {0.0f, 0.0f};
      sf::Vector2f z = {0.0f, 0.0f};
      sf::Vector2f w = {0.0f, 0.0f};

      sf::VertexArray vertices;
      sf::CircleShape centerDot;
      sf::VertexArray outline;
      sf::Text label;
      sf::Texture texture;
  };
}

#endif // _PMD_TILE