#ifndef _PMD_SCENE
#define _PMD_SCENE

#include <typeinfo>

#include <SFML/Graphics.hpp>

#include "InputManager.h"

namespace pmd
{
  class SceneManager;

  using SceneId = size_t;

  // base class for pointers
  class BaseScene
  {
    public:
      BaseScene(SceneManager *mgr) : manager{mgr} {};
      virtual ~BaseScene() {};

      virtual bool init() = 0;
      virtual void handleInput(const InputManager& input) = 0;
      virtual void update(double dt) = 0;
      virtual void draw(sf::RenderTarget& target) = 0;

      SceneManager *manager;
  };

  // crtp for scene id initialization
  template <typename T>
  class Scene : public BaseScene
  {
    public:
      explicit Scene(SceneManager *mgr) : BaseScene{mgr}
      {
        id = typeid(T).hash_code();
      };

      virtual ~Scene() {};

      static SceneId id;
  };

  template <typename T>
  SceneId Scene<T>::id;
}

#endif // _PMD_SCENE