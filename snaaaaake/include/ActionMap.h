#ifndef _PMD_ACTIONMAP
#define _PMD_ACTIONMAP

#include <algorithm>
#include <array>
#include <iterator>
#include <map>

#include "Inputs.h"
#include "InputManager.h"
#include "Actions.h"

using namespace std;

namespace pmd
{
  class ActionMap
  {
    public:
      ActionMap() = default;

      void process(const InputManager& inputs)
      {
        clearOldActionStates();
        for (const auto& kv : keymap)
        {
          bool allKeysPressed = all_of(begin(kv.first), end(kv.first), [&](KeyCode k){ return inputs.isPressed(k); });
          if (allKeysPressed)
          {
            actions[kv.second] = ActionState::Active;
          }
        }
      };

      bool anyInput()
      {
        return any_of(begin(actions), end(actions), [](ActionState s){ return s == ActionState::Active; });
      };

      inline bool isActive(Action a)
      {
        return actions[a] == ActionState::Active;
      };

      void clearOldActionStates()
      {
        memset(&actions, ActionState::Inactive, sizeof(actions));
      };

      map<KeyCombo, Action> keymap;
      array<ActionState, Action::MaxActions> actions;
  };
}

#endif // _PMD_ACTIONMAP